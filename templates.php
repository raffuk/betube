<?php

	function createHtmltable($data)
	{
		$output = "";

		if (count($data) > 0){
		
			$output .=  "<table><thead><tr><th>";
			$output .=  implode('</th><th>', array_keys(current($data)));
			$output .=  "</th></tr></thead><tbody>";

			foreach ($data as $row){
				array_map('htmlentities', $row); 
				$output .= "<tr><td>";
				$output .= implode('</td><td>', $row);
				$output .= "</td></tr>";
			}
			
			$output .= "</tbody></table>";
		}

		return $output;
	}

	function createPerformancetable($history)
	{
		
		$videos_readyforreview = count($history[READY_FOR_REVIEW])>0 ? "(".count($history[READY_FOR_REVIEW]).")": "";
		
		if(count($history[READY_FOR_REVIEW])>4)
			$videos_readyforreview = "<span style='color:red'>".$videos_readyforreview."</blink>";
		
		$_link_readyforreview = "<a href='".LINK_BOARDS_IN_QA."' target='_blank'>Ready for Review</a>";
		$output =<<<EOF
<table class="table table-bordered table-striped table-hover">
			<tbody>
				<thead>
					<tr><h3>WORKLOAD: Videos In Progress and Ready for Review</h3></tr>
				</thead>
			<tr>
			    <th colspan="2"></th>
			    <th colspan="5">$_link_readyforreview $videos_readyforreview</th>
			</tr>
			<tr>
				<th>Editor</th>
			    <th>In Progress</th> 
				
			    <th>Today</th>
			    <th>This Week</th>
			    <th>Last Week</th>
			    <th>This Month</th>
			    <th>Last Month</th>
			</tr>
EOF;
		foreach ($history as $key => $value) {
			if(empty($key) || ($value[READY_FOR_REVIEW_THIS_MONTH][TOTAL] == 0))
				continue;

			$last_update = (isset($value[JIRA_STATUS_IN_PROGRESS][USER_LAST_ACCESS])) ? time_elapsed_string($value[JIRA_STATUS_IN_PROGRESS][USER_LAST_ACCESS]) : "?";

			$output .=  "<tr class='".$color."'>";
				$output .=  "<td>".$key." (".$last_update.")"."</td>";
				
				$output .=  "<td>";
					$output .=  ($value[JIRA_STATUS_IN_PROGRESS][TOTAL]) ? $value[JIRA_STATUS_IN_PROGRESS][TOTAL] : 0; 
				$output .=  "</td>";
				
				$output .=  "<td>";
					$output .=  ($value[READY_FOR_REVIEW_TODAY][TOTAL]) ? $value[READY_FOR_REVIEW_TODAY][TOTAL] : 0; 
				$output .=  "</td>";
				
				$output .=  "<td>";
					$output .=  ($value[READY_FOR_REVIEW_THIS_WEEK][TOTAL]) ? $value[READY_FOR_REVIEW_THIS_WEEK][TOTAL] : 0; 
				$output .=  "</td>";
				
				$output .=  "<td>";
					$output .=  ($value[READY_FOR_REVIEW_LAST_WEEK][TOTAL]) ? displayAverage($value[READY_FOR_REVIEW_LAST_WEEK][TOTAL],5) : 0; 
				$output .=  "</td>";
				
				$output .=  "<td>";
					$output .=  ($value[READY_FOR_REVIEW_THIS_MONTH][TOTAL]) ? $value[READY_FOR_REVIEW_THIS_MONTH][TOTAL] : 0; 
				$output .=  "</td>";
				
				$output .=  "<td>";
					$output .=  ($value[READY_FOR_REVIEW_LAST_MONTH][TOTAL]) ? displayAverage($value[READY_FOR_REVIEW_LAST_MONTH][TOTAL],20) : 0; 
				$output .=  "</td>";
				
			$output .=  "</tr>";
		}

		$output .= "</table>";

	return $output;	
}

function createChanneltable($history)
	{
	
	$totaldata = new stdClass;
	foreach($history as $totalkey => $totalvalue)
	{
		$totaldata->readyforrelease += count($totalvalue[JIRA_STATUS_QA][TOTAL]);
		$totaldata->prepareforrelease += count($totalvalue[JIRA_STATUS_PREPARE_FOR_RELEASE][TOTAL]);
		$totaldata->release += count($totalvalue[JIRA_STATUS_RELEASE][TOTAL]);
	}
// 	p($totaldata);
	$prepareforrelease = !EMPTY($totaldata->prepareforrelease) ? "<span title='".$totaldata->prepareforrelease." videos that <b>needs to be uploaded to gDrive.</b>'>(".$totaldata->prepareforrelease.")</span>" : "";
		$_link_readyforrelease = "<a href='".LINK_BOARDS_READY_FOR_RELEASE."' target='_blank'>Ready for Release</a>";
		$output =<<<EOF
<table class="table table-bordered table-striped table-hover">
			<tbody>
				<thead>
					<tr><h3>RELEASES</h3></tr>
				</thead>
			<tr>
				<th>Channel</th>
			    <th>In Progress</th> 
			    <th>Ready for Review</th> 
			    <th>$_link_readyforrelease $prepareforrelease</th> 
			    <th>Scheduled</th>
			    <th>Last Release</th>
			    <th>Next</th>
			    <th>Latest</th>
			</tr>
EOF;

// Include later: 
// 			    <th>This Month</th>
// 			    <th>Last Month</th>

		foreach ($history as $key => $value) {
			if(empty($key))
				continue;
			
			$status_points = $value[JIRA_STATUS_PREPARE_FOR_RELEASE][TOTAL] + $value[JIRA_STATUS_READY_FOR_RELEASE][TOTAL] + $value[JIRA_STATUS_RELEASE][TOTAL];
			
			switch($status_points){
				case 0:
					$color = "red";			
					break;	
				case 1:
				case 2:
				case 3:
					$color = "orange";			
					break;	
				case 4:
					$color = "yellow";					
					break;	
				case 5:
					$color = "Olive";					
					break;	
				case 6:
				case 7:
				case 8:
				case 9:
				case 10:
					$color = "Green";	
					break;			
				default:
					$color = "while";		
					break;
			}
			
			//Last check if RELEASED = 0 = RED ALERT

			if($value[JIRA_STATUS_RELEASE][TOTAL] <= 3)
				$color = "orange";
			
			if($value[JIRA_STATUS_RELEASE][TOTAL] == 0)
				$color = "red";
				
				
			//p($value);
			//p($history);

			$output .=  "<tr style='background-color:".$color."'>";
				$output .=  "<td>".$key."</td>";

				$output .=  "<td>";
					$output .=  ($value[JIRA_STATUS_IN_PROGRESS][TOTAL]) ? $value[JIRA_STATUS_IN_PROGRESS][TOTAL] : 0; 
				$output .=  "</td>";
				$output .=  "<td>";
					$output .=  ($value[JIRA_STATUS_QA][TOTAL]) ? $value[JIRA_STATUS_QA][TOTAL] : 0; 
				$output .=  "</td>";
				
				$ticket_details = "";
			//	p($value,JIRA_STATUS_PREPARE_FOR_RELEASE);
					if($value[JIRA_STATUS_PREPARE_FOR_RELEASE][TOTAL] > 0)
						foreach ($value as $statuskey => $statusvalue)
						{
							if(($statuskey == JIRA_STATUS_PREPARE_FOR_RELEASE) && (isset($statusvalue[TICKETS])))
								foreach($statusvalue[TICKETS] as $ticketsdetailskey => $ticketdetailsvalue)
							{
							
								$ticket_details .= (!empty($ticket_details))? "\n" : "";
								$ticket_details .= $ticketsdetailskey. ": ". $ticketdetailsvalue;
								$ticketstoupload[$ticketsdetailskey] = $ticketdetailsvalue;
							}
						}

				$output .=  "<td>";
				$output .=  "<span title='".$ticket_details."'>";
					$output .= ($value[JIRA_STATUS_PREPARE_FOR_RELEASE]) ? "(".$value[JIRA_STATUS_PREPARE_FOR_RELEASE][TOTAL].")" : "";
					$output .=  ($value[JIRA_STATUS_READY_FOR_RELEASE][TOTAL]) ? $value[JIRA_STATUS_READY_FOR_RELEASE][TOTAL] : 0; 
				$output .= "</span>";
					

					
				//	p($ticketdetails, "ticket details");
				$output .=  "</td>";
				
				$output .=  "<td>";
					$output .=  ($value[JIRA_STATUS_RELEASE][TOTAL]) ? $value[JIRA_STATUS_RELEASE][TOTAL] : 0; 
				$output .=  "</td>";
				
				$output .=  "<td>";
					$output .=  ($value[JIRA_STATUS_RELEASE][RELEASED_LAST]) ? time_elapsed_string($value[JIRA_STATUS_RELEASE][RELEASED_LAST]) : ""; 		
				$output .=  "</td>";
				$output .=  "<td>";
					$output .=  ($value[JIRA_STATUS_RELEASE][NEXT_RELEASE]) ? $value[JIRA_STATUS_RELEASE][NEXT_RELEASE]."(".time_future_string($value[JIRA_STATUS_RELEASE][NEXT_RELEASE]).")" : ""; 		
				$output .=  "</td>";
				
				$output .=  "<td>";
					$output .=  ($value[JIRA_STATUS_RELEASE][LAST_RELEASE]) ? $value[JIRA_STATUS_RELEASE][LAST_RELEASE]." (".time_future_string($value[JIRA_STATUS_RELEASE][LAST_RELEASE]).")" : ""; 	
				$output .=  "</td>";
				
				
			$output .=  "</tr>";
		}

		$output .= "</table>";

		$slackmsg .= "Guys, please upload the following videos to Google Drive: \n";

		//$slackmsg .= "thste</br>tasdsaas</br>\n beasdadas";
		if(is_array($ticketstoupload))
		foreach($ticketstoupload as $key => $value)
		{
			$slackmsg .= $key." -> ".$value.", ";
		}

		$output .= "<a href='slack.php?msg=".$slackmsg."' target='_blank'>Send Slack with missing uploads</a>";

	return $output;	
}

?>