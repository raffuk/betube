<!DOCTYPE html>
<html lang="en">
<head>
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BeTube 1.0</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="gridstack.js/dist/gridstack.css"/>
     <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.0/jquery-ui.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.5.0/lodash.min.js"></script>
    <script src="gridstack.js/dist/gridstack.js"></script>
    <script src="gridstack.js/dist/gridstack.jQueryUI.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.min.js"></script>
    
    <style type="text/css">

    /*
		WHITE: #FEFDFF
		SILVER: #C1BFB5
		ORANGE RED: #EB4511
		RED RUST: #B02E0C
		PEWTER BLUE: #8EB1C7


    */
        .grid-stack {
            background: #FEFDFF;
        }

        .grid-stack-item-content {
            color: #2c3e50;
            text-align: center;
            background-color: #D5E2EA;
        }

        .grid-stack .grid-stack {
            /*margin: 0 -10px;*/
            background: #E8E7E4;
        }

        .grid-stack .grid-stack .grid-stack-item-content {
            background: #F7BBA8;
        }

    	#in_progress_assignees {
		    display: none;
		}
		 	.alert-warning{
				font-size:11px;
				text-align:left;
				padding: 5px;
			}
			body{
		 		font-size:12px;
			}
		  #panel{
		    display:block;
		  }
		  #videopreparation{
		    float:left;
		    width: 50%;
		  }

		  #videopreview{
		    float:right;
		    width: 50%;
		  }

		  .container{
		    width: 100%;
		    padding: 5px;

		  }

		  .videoprep{
		    float:left;
		    width: 50%;
		  }
		  .videopreview{
		    float:right;
		    /*width: 50%;*/
		  }
		  
		  #youtube_preview {
		    min-height: 20%;
		  }
		.keywords {
		    width: 50%;
		    float: left;
		}
		#save_jira {
		  width: 100%;
		  height: 60px;
		}
		.updatejira {
		    float: right;
		    display: block;
		    max-width: 50%;
		    height: 60px;
		}
		.panel-title .prep{
		  float:left;
		}

		.sources{
		  float:right;
		  width: 50%;
		  display: block;
		}
		.sources button{
		  width: 100%;
		}

		.list select{
		  width: 100%;
		}

		.list{
		  float:left;
		  width: 50%;
		  display: block;
		}
		textarea, input, select{
		  font-size:11px !important;
		}
		button {
		  font-size:12pt !important;
		}
		.glyphicon {
		  
		  cursor: pointer;
		}
		select .selectpicker{
		  width: 100%;
		  display: block;
		}
		.visuals{
		  display: block;
		  width: 100%;
		  padding:3;
		}

		.form-group .youtube{
		  display: block;
		  float:left;
		  width: 70%;

		}
		.form-group .images{
		  display: block;
		  float:right;
		  width: 30%;
		  padding: 3px;
		}
		.form-group .images img{  
		  width: 100%;
		  height: 100%;
		}
    </style>
</head>

<?php
require_once("constants.php");
//pull in login credentials and CURL access function
require_once("jira_php/utils.php");
//call all the functions 
require_once("functions.php");
require_once("classes.php");

$reviews = new Reviews();
//p($reviews);
//$slack = new Slack();
?>
<body>
<?php   

        //echo createReleaseScreen($releases);
?>
  <div class="container-fluid">
        <h1>BeTube</h1>

        <div class="grid-stack">
            <div class="grid-stack-item" data-gs-x="0" data-gs-y="0" data-gs-width="4" data-gs-height="3">
                <div class="grid-stack-item-content">
                    <?php print $dropdown; ?>
                </div>
            </div>
            <div class="grid-stack-item" data-gs-x="0" data-gs-y="0" data-gs-width="4" data-gs-height="3">
                <div class="grid-stack-item-content">
                    2
                </div>
            </div>
            <!-- <div class="grid-stack-item" data-gs-x="4" data-gs-y="0" data-gs-width="4" data-gs-height="4">
                <div class="grid-stack-item-content">
                            2nd
                        <div class="grid-stack">
                            <div class="grid-stack-item" data-gs-x="0" data-gs-y="0" data-gs-width="3" data-gs-height="1"><div class="grid-stack-item-content">1</div></div>
                            <div class="grid-stack-item" data-gs-x="3" data-gs-y="0" data-gs-width="3" data-gs-height="1"><div class="grid-stack-item-content">2</div></div>
                            <div class="grid-stack-item" data-gs-x="6" data-gs-y="0" data-gs-width="3" data-gs-height="1"><div class="grid-stack-item-content">3</div></div>
                            <div class="grid-stack-item" data-gs-x="9" data-gs-y="0" data-gs-width="3" data-gs-height="1"><div class="grid-stack-item-content">4</div></div>
                            <div class="grid-stack-item" data-gs-x="0" data-gs-y="1" data-gs-width="3" data-gs-height="1"><div class="grid-stack-item-content">5</div></div>
                            <div class="grid-stack-item" data-gs-x="3" data-gs-y="1" data-gs-width="3" data-gs-height="1"><div class="grid-stack-item-content">6</div></div>
                        </div>

                </div> -->
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function () {
            var options = {
            };
            $('.grid-stack').gridstack(options);
        });
    </script>
</body>
</html>
