<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>BeTube - Channels Management</title>

    <!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.min.css" rel="stylesheet">
    
	<style>
	
	</style>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  </head>

<?php
  include("constants.php");  
	include("functions.php");  
	include("classes.php");    
	  $channels = Database::get("Channels");
//p($_POST);
	  if(isset($_POST["Jira_Key"]))
	  {
	  	$fields  = "name = \"".$_POST["Channel_Name"]."\",";
      $fields  .= "jira_key = \"".$_POST["Jira_Key"]."\",";
	  	$fields  .= "release_days = \"".$_POST["Release_Days"]."\",";
	  	$fields  .= "channel_link = \"".$_POST["Channel_Link"]."\",";
	  	$fields  .= "facebook_link = \"".$_POST["Facebook_Link"]."\",";
	  	$fields  .= "bestin = \"".$_POST["Best_In"]."\",";
	  	$fields  .= "headlight = \"".$_POST["Headlight"]."\",";
	  	$fields  .= "tags = \"".$_POST["Tags"]."\",";
	  	$fields  .= "links = \"".$_POST["Links"]."\"";
	        		
	  	print Database::save("Channels", $fields)." rows updated";

	  }	
       ?>
  <body>

    <div class="container">
      <div id="panel">
      	<form id="channelsManagement" name="channelsManagement" action="#" METHOD='GET'>
        <select name="channel" id="channel" onchange="submit();">
        	<?php         	
        	foreach ($channels as $key => $value) {	
        		if($value->jira_key == $_GET['channel'])
        			$select = "selected";
        		else
        			$select = "";
        		print "<option value='".$value->jira_key."' ".$select.">".$value->name."</option>";        		
        	}
        	?>
        </select>
        </form>
        <form id="changeChannels" name="changeChannels" action="#" METHOD='POST'>
        <?php 
        	$selected_channel = isset($_GET['channel']) ? $_GET['channel'] : "ADRENA";

        		$channel = Database::get("Channels", "*", "jira_key = '".$selected_channel."'");
        		//p($channel);
        		foreach ($channel as $key => $value) {
        		//	p($value,$key);
	        		$fields = _formgroupInput("Jira Key", $value->jira_key);
	        		$fields .= _formgroupInput("Channel Name", $value->name);
	        		$fields .= _formgroupTextarea("Tags", $value->tags);
	        		$fields .= _formgroupInput("Release Days", $value->release_days);
	        		$fields .= _formgroupInput("Channel Link", $value->channel_link);
	        		$fields .= _formgroupInput("Facebook Link", $value->facebook_link);
	        		$fields .= _formgroupInput("Best In", $value->bestin);
	        		$fields .= _formgroupInput("Headlight", $value->headlight);
	        		
	        		$fields .= _formgroupTextarea("Links", $value->links);
	        	}
	        	print $fields;
        	
//p($_GET);
        ?>
        <input type="submit" value="Save">
</form>
      </div>
    </div>  
   
  </body>
</html>