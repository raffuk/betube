<?php
	date_default_timezone_set('America/Sao_Paulo');
	function p($string, $title = false) {
		print("<pre>");
		print("<h4>".$title.":</h4>");
		print_r($string);
		print("</pre>");
	}
	
	function sendSlack($msg)
	{
  // Create a constant to store your Slack URL
 	 define('SLACK_WEBHOOK', 'https://hooks.slack.com/services/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');
  // Make your message
 	 $message = array('payload' => json_encode(array('text' => $msg)));
  // Use curl to send your message
 	 $c = curl_init(SLACK_WEBHOOK);
	  curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
 	 curl_setopt($c, CURLOPT_POST, true);
 	 curl_setopt($c, CURLOPT_POSTFIELDS, $message);
 	 curl_exec($c);
 	 curl_close($c);
	}
	function createHtmltable($data)
	{
		$output = "";

		if (count($data) > 0){
		
			$output .=  "<table><thead><tr><th>";
			$output .=  implode('</th><th>', array_keys(current($data)));
			$output .=  "</th></tr></thead><tbody>";

			foreach ($data as $row){
				array_map('htmlentities', $row); 
				$output .= "<tr><td>";
				$output .= implode('</td><td>', $row);
				$output .= "</td></tr>";
			}
			
			$output .= "</tbody></table>";
		}

		return $output;
	}

	function displayAverage($value, $dividend)
	{
		return $value." (~".average($value, $dividend)."/day)";
	}

	function average($value, $dividend)
	{
		return number_format($value/$dividend,2);
	}
	
	function time_elapsed_string($datetime, $full = false, $hours = false) 
	{
		//$datetime = "2017-01-10T20:21:04.000+0100";
	    $now = new DateTime;
	    $ago = new DateTime($datetime);
	    $diff = $now->diff($ago);

	    $diff->w = floor($diff->d / 7);
	    $diff->d -= $diff->w * 7;

	    $string = array(
	        'y' => 'year',
	        'm' => 'month',
	        'w' => 'week',
	        'd' => 'day'
	    );
	    
	    if($hours = true)
	    	array_push($string, array(
	    						'h' => 'hour',
						        'i' => 'minute',
						        's' => 'second'));
// 	        p($diff,string);
	    foreach ($string as $k => &$v) {
	        if ($diff->$k) {
	            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
	        } else {
	            unset($string[$k]);
	        }
	    }
	  // 	p($ago);
	 //   p($now);
	 //   p($diff, $ago->date);

		if($diff->days==0 && $diff->invert==1)
			return 'Today';
		elseif($diff->days==0 && $diff->invert==0)
			return 'Tomorrow';
		elseif($diff->days==1 && $diff->invert==1)
			return 'Yesterday';
		elseif($diff->days>1 && $diff->invert==0)
			return $string ? "in ".implode(', ', $string)  : 'just now';
	    else
	    	if (!$full) $string = array_slice($string, 0, 1);
	    	return $string ? implode(', ', $string) . ' ago' : 'just now';
	}
	
	function time_future_string($datetime, $full = false, $hours = true) 
	{
	    $now = new DateTime;
	    $ago = new DateTime($datetime);
	    $diff = $ago->diff($now);

	    $diff->w = floor($diff->d / 7);
	    $diff->d -= $diff->w * 7;

	    $string = array(
	        'y' => 'year',
	        'm' => 'month',
	        'w' => 'week',
	        'd' => 'day'
	    );
	    
	    if($hours = true)
	    	array_push($string, array(
	    						'h' => 'hour',
						        'i' => 'minute',
						        's' => 'second'));
	        
	    if($diff->d==0)
			return 'Tomorrow';    
	    
	    foreach ($string as $k => &$v) {
	        if ($diff->$k) {
	            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
	        } else {
	            unset($string[$k]);
	        }
	    }

	    if (!$full) $string = array_slice($string, 0, 1);
	    
	    return $string ? 'In '. implode(', ', $string)  : 'Soon';
	}

	function createPerformancetable($history)
	{

		$videos_readyforreview = count($history[READY_FOR_REVIEW])>0 ? count($history[READY_FOR_REVIEW]): "";
		

		$videos_readyforreview = "<span class='badge'>".$videos_readyforreview."</span>";
		
		$_link_readyforreview = "<a href='".LINK_BOARDS_IN_QA."' target='_blank'>Ready for Review</a>";
		$output =<<<EOF
<table class="table table-bordered table-striped table-hover">
			<tbody>
				<thead>
					<tr><h3>WORKLOAD: Videos In Progress and Ready for Review</h3></tr>
				</thead>
			<tr>
			    <th colspan="2"></th>
			    <th colspan="5">$_link_readyforreview $videos_readyforreview</th>
			</tr>
			<tr>
				<th>Editor</th>
			    <th>Backlog</th> 
			    <th>Pipeline</th> 
			    <th>In Progress</th> 
				
			    <th>Today</th>
			    <th>This Week</th>
			    <th>Last Week</th>
			    <th>This Month</th>
			    <th>Last Month</th>
			</tr>
EOF;
		
		foreach ($history as $key => $value) {
			if(empty($key) || ($value[READY_FOR_REVIEW_LAST_MONTH][TOTAL] + $value[READY_FOR_REVIEW_THIS_MONTH][TOTAL] + $value[JIRA_STATUS_IN_PROGRESS][TOTAL] == 0))
				continue;

			$last_update = (isset($value[JIRA_STATUS_IN_PROGRESS][USER_LAST_ACCESS])) ? time_elapsed_string($value[JIRA_STATUS_IN_PROGRESS][USER_LAST_ACCESS]) : "?";

			$output .=  "<tr class='".$color."'>";
				$output .=  "<td>".$key." (".$last_update.")"."</td>";
				$output .=  "<td>".$value[JIRA_STATUS_BACKLOG][TOTAL]."</td>";
				$output .=  "<td>".$value[JIRA_STATUS_PIPELINE][TOTAL]."</td>";

				$output .=  "<td>";
					$output .=  ($value[JIRA_STATUS_IN_PROGRESS][TOTAL]) ? $value[JIRA_STATUS_IN_PROGRESS][TOTAL] : 0; 
				$output .=  "</td>";
				
				$output .=  "<td>";
					$output .=  ($value[READY_FOR_REVIEW_TODAY][TOTAL]) ? $value[READY_FOR_REVIEW_TODAY][TOTAL] : 0; 
				$output .=  "</td>";
				
				$output .=  "<td>";
					$output .=  ($value[READY_FOR_REVIEW_THIS_WEEK][TOTAL]) ? $value[READY_FOR_REVIEW_THIS_WEEK][TOTAL] : 0; 
				$output .=  "</td>";
				
				$output .=  "<td>";
					$output .=  ($value[READY_FOR_REVIEW_LAST_WEEK][TOTAL]) ? displayAverage($value[READY_FOR_REVIEW_LAST_WEEK][TOTAL],5) : 0; 
				$output .=  "</td>";
				
				$output .=  "<td>";
					$output .=  ($value[READY_FOR_REVIEW_THIS_MONTH][TOTAL]) ? $value[READY_FOR_REVIEW_THIS_MONTH][TOTAL] : 0; 
				$output .=  "</td>";
				
				$output .=  "<td>";
					$output .=  ($value[READY_FOR_REVIEW_LAST_MONTH][TOTAL]) ? displayAverage($value[READY_FOR_REVIEW_LAST_MONTH][TOTAL],20) : 0; 
				$output .=  "</td>";
				
			$output .=  "</tr>";
		}

		$output .= "</table>";

	return $output;	
}

function createChanneltable($history)
	{

	$totaldata = new stdClass;

	$pendingUploads = "<table><tr><th><h2>Pending Google Drive Upload</h2></th></tr>";
	foreach ($history as $historykey => $historyvalue) {
		foreach ($historyvalue as $statuskey => $statusvalue) {
			if($statuskey == JIRA_STATUS_PREPARE_FOR_RELEASE)
				foreach ($statusvalue as $ticketkey => $ticketvalue) {
					if($ticketkey == TICKETS)
						foreach ($ticketvalue as $assigneekey => $assigneevalue)
							$pendingUploads .= "<tr><td>$assigneekey</td><td>$assigneevalue</td><tr>";
						
					
				}
		}
	}
	$pendingUploads .= "</table>";
// 	p($totaldata);
	$prepareforrelease = !EMPTY($ticketstoupload) ? "<span class='badge' title='".count($ticketstoupload)." videos that <b>needs to be uploaded to gDrive.</b>'>".count($ticketstoupload)."</span>" : "";
		$_link_readyforrelease = "<a href='".LINK_BOARDS_READY_FOR_RELEASE."' target='_blank'>Ready for Release & Scheduled</a>";
		
		$output =<<<EOF
		$pendingUploads
<table class="table table-bordered table-striped table-hover">
			<tbody>
				<thead>
					<tr><h3>RELEASES</h3></tr>
				</thead>
			<tr>
				<th>Needs Release</th>
				<th>Channel</th>
				<th>Backlog & Pipeline</th>
			    <th>$_link_readyforrelease $prepareforrelease</th> 
			    <th>Last Release</th>
			    <th>Next Release</th>
			    <th>Latest</th>
			    <th>Last Month</th>
			    <th>This Month</th>
			</tr>
EOF;

		foreach ($history as $key => $value) {
			if(empty($key))
				continue;
			
			$status_points = $value[JIRA_STATUS_QA][TOTAL] + 
							 $value[JIRA_STATUS_READY_FOR_REVIEW][TOTAL] + 
							 $value[JIRA_STATUS_PREPARE_FOR_RELEASE][TOTAL] + 
							 $value[JIRA_STATUS_READY_FOR_RELEASE][TOTAL] + 
							 $value[JIRA_STATUS_RELEASED][TOTAL] + 
							 $value[JIRA_STATUS_IN_PROGRESS][TOTAL];
			
			switch($status_points){
				case 0:
					$color = "red";
					break;
				case 1:
				case 2:
					$color = "#D32F2F";			
					break;					
				case 3:
				case 4:
				case 5:
					$color = "#FFCDD2";					
					break;				
				default:
					$color = "white";		
					break;
			}
			
			//Last check if RELEASED = 0 = RED ALERT
			
			if(($value[JIRA_STATUS_RELEASED][TOTAL] == 0) && (time_elapsed_string($value[JIRA_STATUS_RELEASE][RELEASED_LAST]) !== "Today"))
				$alert = "<span style='font-weight: bold;' class='glyphicon glyphicon-fire' aria-hidden='true'>";
			else
				$alert ="<span>";
			
			$total_backlog = count($value[JIRA_STATUS_BACKLOG][TICKETS]);
			$total_pipeline = count($value[JIRA_STATUS_PIPELINE][TICKETS]);


			$total_in_progress = count($value[JIRA_STATUS_IN_PROGRESS][TICKETS]);
			
			$total_qa = count($value[JIRA_STATUS_QA][TICKETS]);
			$total_ready_for_review = count($value[JIRA_STATUS_READY_FOR_REVIEW][TICKETS]);
			//p($value[JIRA_STATUS_PREPARE_FOR_RELEASE], $key." prepare for release");
			$total_prepare_for_release = count($value[JIRA_STATUS_PREPARE_FOR_RELEASE][TICKETS]);
			$total_ready_for_release = count($value[JIRA_STATUS_READY_FOR_RELEASE][TICKETS]);
			
			$total_scheduled = count($value[JIRA_STATUS_RELEASED][TICKETS]);

			$percentage_total = $total_in_progress 
								+ $total_qa 
								+ $total_ready_for_review 
								+ $total_prepare_for_release 
								+ $total_ready_for_release 
								+ $total_scheduled;	
			$backlog_total = $total_backlog 
								+ $total_pipeline ;					
			
			$percentage_backlog = ($total_backlog>0) ? 100 / $backlog_total * $total_backlog : 0;
			$percentage_pipeline = ($total_pipeline>0) ? 100 / $backlog_total * $total_pipeline : 0;
			
			$percentage_in_progress = ($total_in_progress>0) ? 100 / $percentage_total * $total_in_progress : 0;
		
			$percentage_qa = ($total_qa>0) ? 100 / $percentage_total * $total_qa : 0;
			
			$percentage_ready_for_review = ($total_ready_for_review>0) ? 100 / $percentage_total * $total_ready_for_review : 0;
		
			$percentage_ready_for_release = ($total_ready_for_release>0) ? 100 / $percentage_total * $total_ready_for_release : 0;
		
			$percentage_prepare_for_release = ($total_prepare_for_release>0) ? 100 / $percentage_total * $total_prepare_for_release : 0;
		
			$percentage_scheduled = ($total_scheduled>0) ? 100 / $percentage_total * $total_scheduled : 0;
			
// 			<span class="label label-default">Default Label</span>
// 			<span class="label label-primary">Primary Label</span>
// 		    <span class="label label-success">Success Label</span>
// 	      	<span class="label label-info">Info Label</span>
// 			<span class="label label-warning">Warning Label</span>
// 			<span class="label label-danger">Danger Label</span>
			
			unset($backlog_summary);
			$backlog_summary =  ($total_backlog>0) ? '<span class="label label-danger progressbar2">'.JIRA_STATUS_BACKLOG." ".$total_backlog."</span> " : "";
			$backlog_summary .= ($total_pipeline>0) ? '<span class="label label-info progressbar2">'.JIRA_STATUS_PIPELINE." ".$total_pipeline."</span> " : "";

			unset($summary);
			$summary =  ($total_in_progress>0) ? '<span class="label label-info progressbar2">'.JIRA_STATUS_IN_PROGRESS." ".$total_in_progress."</span> " : "";
			$summary .= ($total_qa>0) ? '<span class="label label-warning progressbar2">'.JIRA_STATUS_QA." ".$total_qa."</span> " : "";
			$summary .= ($total_ready_for_review>0) ? '<span class="label label-warning progressbar2">'.JIRA_STATUS_READY_FOR_REVIEW." ".$total_ready_for_review."</span> " : "";
			$summary .= ($total_prepare_for_release>0) ? '<span class="label label-danger progressbar2">'.JIRA_STATUS_PREPARE_FOR_RELEASE." ".$total_prepare_for_release."</span> " : "";
			$summary .= ($total_ready_for_release>0) ? '<span class="label label-danger progressbar2">'.JIRA_STATUS_READY_FOR_RELEASE." ".$total_ready_for_release."</span> " : "";
			$summary .= ($total_scheduled>0) ? '<span class="label label-success progressbar2">'.JIRA_STATUS_SCHEDULED." ".$total_scheduled."</span> " : "";
			
			$backlog_pipeline_bar = '
			<div class="progress">
			  <div title="'.$total_backlog.' '.JIRA_STATUS_BACKLOG.'" class="progress-bar progress-bar-danger" role="progressbar" style="width:'.$percentage_backlog.'%">
			    
			  </div> <div title="'.$total_pipeline.' '.JIRA_STATUS_PIPELINE.'" class="progress-bar progress-bar-info" role="progressbar" style="width:'.$percentage_pipeline.'%">
			    
			  </div>			  
			</div>
			<div>'.$backlog_summary.'</div>';

			$ready_for_release_scheduled_progress_bar = '
			<div class="progress">
			  <div title="'.$total_in_progress.' '.JIRA_STATUS_IN_PROGRESS.'" class="progress-bar progress-bar-info" role="progressbar" style="width:'.$percentage_in_progress.'%">
			    In Progress
			  </div> <div title="'.$total_qa.' '.JIRA_STATUS_QA.'" class="progress-bar progress-bar-warning" role="progressbar" style="width:'.$percentage_qa.'%">
			    In QA
			  </div>
			  <div title="'.$total_ready_for_review.' '.JIRA_STATUS_READY_FOR_REVIEW.'" class="progress-bar progress-bar-warning" role="progressbar" style="width:'.$percentage_ready_for_review.'%">
			    Ready for Review
			  </div>
			  <div title="'.$total_ready_prepare_for_release.' '.JIRA_STATUS_PREPARE_FOR_RELEASE.'" class="progress-bar progress-bar-danger" role="progressbar" style="width:'.$percentage_prepare_for_release.'%">
			    Prepare for Release
			  </div> 
			  <div title="'.$total_ready_for_release.' '.JIRA_STATUS_READY_FOR_RELEASE.'" class="progress-bar progress-bar-danger" role="progressbar" style="width:'.$percentage_ready_for_release.'%">
			    Ready for Release
			  </div>
			  <div title="'.$total_scheduled.' '.JIRA_STATUS_SCHEDULED.'" class="progress-bar progress-bar-success" role="progressbar" style="width:'.$percentage_scheduled.'%">
			    Scheduled
			  </div>
			  
			</div>
			<div>'.$summary.'</div>';

			//p($value);
			//p($history);
			

			$output .=  "<tr style='background-color:".$color."'>";
				$output .=  "<td>".$alert."</span></td>";
				$output .=  "<td>".$key."</td>";
			
				$output .=  "<td>";
				$output .= $backlog_pipeline_bar;
				$output .=  "</td>";

				$output .=  "<td>";
				$output .= $ready_for_release_scheduled_progress_bar;
					
				$output .=  "</td>";
			
			//	p($value[JIRA_STATUS_RELEASE][RELEASED_LAST], $key); 
				$output .=  "<td>";
					$output .=  (isset($value[JIRA_STATUS_RELEASE][RELEASED_LAST])) ? time_elapsed_string($value[JIRA_STATUS_RELEASE][RELEASED_LAST]) : ""; 		
				$output .=  "</td>";
				$output .=  "<td>";
					$output .=  ($value[JIRA_STATUS_RELEASE][NEXT_RELEASE]) ? "<span title='".$value[JIRA_STATUS_RELEASE][NEXT_RELEASE]."'>".time_elapsed_string($value[JIRA_STATUS_RELEASE][NEXT_RELEASE])."</span>" : ""; 		
				$output .=  "</td>";
		
				$output .=  "<td>";
					$output .=  ($value[JIRA_STATUS_RELEASE][LAST_RELEASE]) ? $value[JIRA_STATUS_RELEASE][LAST_RELEASE]." (".time_elapsed_string($value[JIRA_STATUS_RELEASE][LAST_RELEASE]).")" : ""; 	
				$output .=  "</td>";
				
				$output .= _inject_status_details(JIRA_STATUS_RELEASED_LAST_MONTH, $value, $key);

				$output .= _inject_status_details(JIRA_STATUS_RELEASED_THIS_MONTH, $value, $key);
			$output .=  "</tr>";
		}

		
		$output .= "</table>";
		

		$slackmsg = '';

		//$slackmsg .= "thste</br>tasdsaas</br>\n beasdadas";
		
		if(is_array($ticketstoupload))
		foreach($ticketstoupload as $key => $value)
		{
			if($value !== $assignee)
			{
				$slackmsg .= " @".$value." please upload: ";
				$slackmsg .= "".LINK_JIRA_BROWSE.$key;
			}
			else
				$slackmsg .= ", ".LINK_JIRA_BROWSE.$key;
			
			$assignee = $value;

		}
		
		$output .= '<a href="slack.php?msg='.$slackmsg.'" target="_blank">Send Slack with missing uploads</a>';
		
		
	//	send_slack($slackmsg, "standup", "#");
	return $output;	
}
function send_slack($message, $room = "standup", $type = "#")
{
	
	$whoroom = $type.$room;
	
	$icon = ":smile:"; 
	$data = "payload=" . json_encode(array(         
	        "channel"       =>  "{$whoroom}",
	        "text"          =>  $message,
	        "icon_emoji"    =>  $icon
	    ));
	$url = "https://hooks.slack.com/services/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"; //your webhook endpoint;
	         
	 
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$result = curl_exec($ch);
	p($message);
	if($result === false)
	{
	    echo 'Curl error: ' . curl_error($ch);
	}
	 
	curl_close($ch);

}
function _inject_status_details($constant,$value, $key)
{
	$_div = md5($constant).$key."_div";
	$_span = md5($constant).$key."_span";
	
	$assignees = "<div id='in_progress_assignees' class='$_div alert alert-warning' role='alert'>";
	
	if(isset($value[$constant][TICKETS]))
		foreach($value[$constant][TICKETS] as $inprogresskey => $inprogressvalue)
		{
// 		p($inprogressvalue,inprogressvalue);
			if(is_array($inprogressvalue))
				foreach($inprogressvalue as $_inprogress_key => $_inprogress_value)
					$assignees .= "<a target='_blank' href='".LINK_JIRA_BROWSE.$inprogresskey."'><b>".ucfirst($_inprogress_key)."</b>: <i>".$_inprogress_value."</i> (".$inprogresskey.")</a></br>";
		}
				
		$assignees .= "</div>";
		
		if($value[$constant][TOTAL]>0)
		{
			$output .= '<script>
				
				$(document).ready(function() {
				$(".'.$_span.'").click(function () {

    //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
 $(".'.$_div.'").slideToggle(500, function () {
        //execute this after slideToggle is done
        //change text of header based on visibility of content div
    });

});
});</script>';
					
			$output .=  "<td><span style='cursor:pointer;'class='$_span'><u>";
			$output .=  (count($value[$constant][TICKETS])>0) ? (count($value[$constant][TICKETS])) : 0; 
			$output .=  $assignees;
			$output .=  "</u></span></</td>";
		}else
			$output .= "<td>0</td>";			
			
		return $output;	
}
function embedYoutube($url, $autoplay = true)
{
	$ytarray=explode("/", $url);
	$ytendstring=end($ytarray);
	$ytendarray=explode("?v=", $ytendstring);
	$ytendstring=end($ytendarray);
	$ytendarray=explode("&", $ytendstring);
	$ytcode=$ytendarray[0];
	if($autoplay !== false)
		$ytcode=$ytendarray[0]."?autoplay=1";

	return "<iframe id='youtube_preview' width=\"100%\" height=\"100%\" src=\"http://www.youtube.com/embed/$ytcode\" frameborder=\"0\" allowfullscreen></iframe>";

}
function createReleaseldropdown($data, $formid = "form")
{
//	p($data);
	$output = "<form id='$formid' action='#' METHOD='GET'>";
	$output .=_inject_dropdown_with_group($data, "video", $_GET["video"]);

	$output .= "</form>";
	return $output;	
}

function _inject_dropdown_with_group($array, $select_name, $selectedChannel = false)
{
	if(empty($array))
		return false;
	
	
	$output = '<select data-live-search="true" class="selectpicker" data-header="'.$channel.'" name="'.$select_name.'" id="'.$select_name.'" onchange="submit();">';
	foreach($array->data->issues AS $key => $value) 
	{ 

		$dropdown[$value->fields->project->name][$value->key] = array(JIRA_FIELD_SUMMARY => $value->fields->summary, 
																	  JIRA_FIELD_STATUS => $value->fields->status->name,
																	  JIRA_FIELD_ASSIGNEE => $value->fields->assignee->name,
																	  JIRA_FIELD_COMMENT => $value->fields->comment,
																	  JIRA_FIELD_KEYWORDS_TITLE => $value->fields->{JIRA_FIELD_KEYWORDS_TITLE});
		
	}
		//p($dropdown,dropdown);

		$firsttime = 0;
		foreach($dropdown as $channel =>$video)
		{
			if($channel != $currentchannel)
			{
				if($firsttime == 1)
					$output .="</optgroup>";

				$output .='<optgroup label="'.$channel.'">';
		
			}

			foreach($video as $videokey => $videovalue)
			{
				if(!empty($videovalue[JIRA_FIELD_KEYWORDS_TITLE]))
					$color = "background-color:lightblue";
				else
					$color = "";
			//	p($videovalue,$key);
				if($selectedChannel == $videokey)
					$select = "selected";
				else
					$select = "";
//p();
			 	$output .='<option style = "'.$color.'" data-subtext="'.$videovalue[JIRA_FIELD_STATUS]." [".$videovalue[JIRA_FIELD_ASSIGNEE]."] (".count($videovalue[JIRA_FIELD_COMMENT]->{JIRA_FIELD_COMMENTS}).')" value="'.$videokey.'" '.$select.'>'.$videovalue[JIRA_FIELD_SUMMARY].'</option>'; 	
			} 	
			$firsttime = 1;

			$currentchannel = $channel;
		}

		$output .= "</select>";

	return $output;
}

function _inject_dropdown($array, $select_name, $selectedChannel = false)
{
	if(empty($array))
		return false;
	
	$output = '<select class="selectpicker" name="'.$select_name.'" id="'.$select_name.'" multiple data-actions-box="true">';
	foreach($array AS $key => $value) 
	{ 
		if(strpos($value,"*") !== false)
		{
			$select = "selected";
			$value = str_replace("*", "", $value);
		}
		else
			$select = "";

		$output .='<option value="'.$value.'" '.$select.'>'.$value.'</option>'; 	
	}

	$output .= "</select>";

	return $output;
}


function _injectCarouselItem($img, $link = false, $active = false, $caption = false, $title = false, $subtitle = false)
{
	$output =<<<EOF
	<div class="item$active">
	<a href="$link" target="_BLANK">
      <img src="$img" alt="$caption">
      <div class="carousel-caption">
        <h3>$title</h3>
        <p>$subtitle</p>
      </div>
      </a>
    </div>
EOF;
	
	return $output;

}
function _injectCarousel()
{
	$output =<<<EOF
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    


  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
EOF;
	
	return $output;
}

function createReleaseScreen($data)
{	
	//p($_POST);
	 if($_POST["hidden_jira_issue_key"])
	 {
	 	$jira_save_fields = array(
			JIRA_FIELD_SUMMARY => $_POST["summary"],
			JIRA_FIELD_YOUTUBE_DESCRIPTION => $_POST["youtubedescription"],
			JIRA_FIELD_YOUTUBE_TAGS => $_POST["youtubetags"],
			JIRA_FIELD_PRIMARY_KEYWORD => $_POST["primarykeyword"],
			JIRA_FIELD_SECONDARY_KEYWORD => $_POST["secondarykeyword"],
			JIRA_FIELD_KEYWORDS_TITLE => $_POST["titlekeyword"],
			JIRA_FIELD_CALL_TO_ACTION => $_POST["calltoaction"],
			JIRA_FIELD_FINAL_VIDEO_LINK => $_POST["videolink"],
			JIRA_FIELD_VIDEO_SOURCE => $_POST["sources"],
			JIRA_FIELD_DESCRIPTION_LIST => $_POST["list"],
			JIRA_FIELD_TAGS => $_POST["tags"],
			JIRA_FIELD_RELEASE_DATE => date("Y-m-d", strtotime($_POST["release_date"]))
		);
		
		$data = new Releases();

	 	$data->save_in_jira($_POST["hidden_jira_issue_key"], $jira_save_fields);
	 	$data->change_status_in_jira($_POST["hidden_jira_issue_key"], JIRA_TRANSITION_RELEASED);

	 	unset($data);
	 	$data = new Releases();
	 	
	 }

	 if(isset($_GET["video"]))
		$video = $_GET["video"];
	
	 if(empty($video))
		$video = $data->data->issues[0]->{JIRA_FIELD_KEY};
	
	$ticket = $data->get_video($video);	

	if(empty($ticket))
		$ticket = $data->get_video($data->data->issues[0]->{JIRA_FIELD_KEY});	
	
	
	$channel = Database::get_channel_data($ticket[JIRA_FIELD_PROJECT]->{JIRA_FIELD_PROJECT_KEY});
	// p($channel);
	$hidden_fields = _injectHiddenField("hidden_jira_issue_key", $ticket[JIRA_FIELD_KEY]);
	$hidden_fields .= _injectHiddenField("hidden_cid", $channel->cid);
	$hidden_fields .= _injectHiddenField("hidden_jira_key", $channel->jira_key);
	$hidden_fields .= _injectHiddenField("hidden_name", $channel->name);
	$hidden_fields .= _injectHiddenField("hidden_tags", $channel->tags);
	$hidden_fields .= _injectHiddenField("hidden_release_days", $channel->release_days);
	$hidden_fields .= _injectHiddenField("hidden_channel_link", $channel->channel_link);
	$hidden_fields .= _injectHiddenField("hidden_facebook_link", $channel->facebook_link);
	$hidden_fields .= _injectHiddenField("hidden_bestin", $channel->bestin);
	$hidden_fields .= _injectHiddenField("hidden_headlight", $channel->headlight);
	$hidden_fields .= _injectHiddenField("hidden_links", $channel->links);
	$hidden_fields .= _injectHiddenField("hidden_template", TEMPLATE_YOUTUBE_DESCRIPTION);
	
	// p($ticket[JIRA_FIELD_PROJECT]->{JIRA_FIELD_PROJECT_KEY}, ticket);
	$JIRA_FIELD_PROJECT_NAME = trim($ticket[JIRA_FIELD_PROJECT]->{JIRA_FIELD_PROJECT_NAME});
	
	$_last_release = Channel::get_last_release($JIRA_FIELD_PROJECT_NAME);
	$last_release = $_last_release->issues[0]->fields->{JIRA_FIELD_RELEASE_DATE}." (".time_elapsed_string($_last_release->issues[0]->fields->{JIRA_FIELD_RELEASE_DATE}).")";
	
	$next_release = date('Y-m-d', strtotime($_last_release->issues[0]->fields->{JIRA_FIELD_RELEASE_DATE}. ' + 2 days'));
	$next_release = date('m/d/Y', strtotime($next_release));
	
	

	$JIRA_FIELD_SUMMARY = trim($ticket[JIRA_FIELD_SUMMARY]);

	$JIRA_FIELD_DESCRIPTION = $ticket[JIRA_FIELD_DESCRIPTION];
	$youtube_embeded = embedYoutube($ticket[JIRA_FIELD_YOUTUBE_URL],false);
	$JIRA_FIELD_GOOGLE_URL = trim($ticket[JIRA_FIELD_GOOGLE_URL]);

	$JIRA_FIELD_RELEASE_DATE = $ticket[JIRA_FIELD_RELEASE_DATE];
	$JIRA_FIELD_FINAL_VIDEO_LINK = $ticket[JIRA_FIELD_FINAL_VIDEO_LINK];
	$JIRA_FIELD_STATUS = $ticket[JIRA_FIELD_STATUS]->{JIRA_FIELD_STATUS_NAME};
	
	$JIRA_FIELD_VIDEO_SOURCE = $ticket[JIRA_FIELD_VIDEO_SOURCE];
	$JIRA_FIELD_AUDIO_SOURCE = $ticket[JIRA_FIELD_AUDIO_SOURCE];
	
	$attachments = $data->get_attachments($video);
	
	$images = "";
	$active= " active";
	$i=0;
	foreach ($attachments->fields->attachment as $key => $value) {
		//p($value);
		$images .= _injectCarouselItem($value->thumbnail,$value->content,$active, $i);
		$active ="";
		$i++;
	}

	//Check if videos source is not empty

	if(!empty($JIRA_FIELD_VIDEO_SOURCE))
	{
		
		$sources_array = explode("\n",$credit_output);

		foreach($sources_array as $value)
			if((strpos($value, 'Video Credits')) || (strpos($value, 'Audio Credits'))) // check if it contains "Videos Sources:" in it and just copy the old one
				$has_no_credit_title = true;
		
		if($has_no_credit_title == true)
			$credit_output = "\nVideo Credits:\n$JIRA_FIELD_VIDEO_SOURCE";	
		else
			$credit_output = $JIRA_FIELD_VIDEO_SOURCE;
	}	

	if(!empty(trim($JIRA_FIELD_AUDIO_SOURCE))) // Check if video + audio source and braek line twice.
		$credit_output .= "\n\nAudio Credits:\n$JIRA_FIELD_AUDIO_SOURCE";
	
	
	$JIRA_FIELD_DESCRIPTION_LIST = $ticket[JIRA_FIELD_DESCRIPTION_LIST];
	$JIRA_FIELD_YOUTUBE_DESCRIPTION = $ticket[JIRA_FIELD_YOUTUBE_DESCRIPTION];
	//p($ticket);

	$branding_tags = explode(",",$channel->tags);
	$JIRA_FIELD_PRIMARY_KEYWORD = trim($ticket[JIRA_FIELD_PRIMARY_KEYWORD]);
	$JIRA_FIELD_SECONDARY_KEYWORD = trim($ticket[JIRA_FIELD_SECONDARY_KEYWORD]);
	$JIRA_FIELD_KEYWORDS_TITLE = trim($ticket[JIRA_FIELD_KEYWORDS_TITLE]);
	$JIRA_FIELD_CALL_TO_ACTION = trim($ticket[JIRA_FIELD_CALL_TO_ACTION]);
	//p($ticket[JIRA_FIELD_ASSIGNEE]);
	
	$JIRA_FIELD_ASSIGNEE_NAME = $ticket[JIRA_FIELD_ASSIGNEE]->{JIRA_FIELD_ASSIGNEE_DISPLAYNAME};
	//p($ticket[JIRA_FIELD_ASSIGNEE]->);
	//p($JIRA_FIELD_ASSIGNEE_NAME);
	$avatar = (array) $ticket[JIRA_FIELD_ASSIGNEE]->{JIRA_FIELD_ASSIGNEE_AVATARURLS};
	//p($avatar);
	//p($ticket[JIRA_FIELD_ASSIGNEE]->{JIRA_FIELD_ASSIGNEE_AVATARURLS});
	//p($ticket[JIRA_FIELD_ASSIGNEE]->{JIRA_FIELD_ASSIGNEE_AVATARURLS}->{'48X48'});
	$avatar_size = "24";
	$JIRA_FIELD_ASSIGNEE_AVATAR = '<img src="'.$avatar[$avatar_size."x".$avatar_size].'" class="img-circle avatar" alt="'.$JIRA_FIELD_ASSIGNEE_NAME.'" width="$avatar_size" height="$avatar_size">';

	$suggested_tags = $data->add_tags(array($JIRA_FIELD_PRIMARY_KEYWORD, $JIRA_FIELD_SECONDARY_KEYWORD));
	$JIRA_FIELD_TAGS = $suggested_tags.",".$ticket[JIRA_FIELD_TAGS];
	
	$JIRA_FIELD_YOUTUBE_TAGS = $ticket[JIRA_FIELD_YOUTUBE_TAGS];

	$dropdown = createReleaseldropdown($data,"release_videos");
//p($branding_tags[0]->tags, branding_tags);
	$branding_tags_dropdown = _inject_dropdown($branding_tags, "branding");

	$link_to_jira = LINK_JIRA_BROWSE.$ticket[JIRA_FIELD_KEY];
	$release_days = $channel->release_days;

	$next_release = (empty($JIRA_FIELD_RELEASE_DATE)) ? $next_release : $JIRA_FIELD_RELEASE_DATE;

	$output =<<<EOF
	
<div class="panel panel-default">
		$dropdown
		
	<div class="panel panel-default" id="videopreview">
		<div class="panel-heading">
            <h3 class="panel-title">$JIRA_FIELD_ASSIGNEE_AVATAR $JIRA_FIELD_ASSIGNEE_NAME</h3>
		</div>
	    <div class="panel-body">
		    <div class="panel-body">

				<form action="#" name="update_jira_form" id="update_jira_form" METHOD="POST">	
				$hidden_fields
				<div class="form-group visuals">
			 		<div class="form-group images">
			            <div id="myCarousel" class="carousel slide" data-ride="carousel">
						  <!-- Indicators -->
						  <!-- Wrapper for slides -->
						  	<div class="carousel-inner" role="listbox">
								$images
						  <!-- Left and right controls -->
							  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
							    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							    <span class="sr-only">Previous</span>
							  </a>
							  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
							    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							    <span class="sr-only">Next</span>
							  </a>
							</div>
						</div>
		            </div>
			 		<div class="form-group youtube">			
		             		$youtube_embeded
		            </div>   
		        
		        </div>
		            
		            <div class="form-group">
		            	<label for="summary"><span class="glyphicon glyphicon-copy" aria-hidden="true" id="copybuttonSummary"></span> Youtube Title (JIRA Summary) <a href="$link_to_jira" target="_blank">JIRA</a></label>
		            	<input name="summary" type="text" class="form-control videopreview" value="$JIRA_FIELD_SUMMARY" id="summary">
		            </div>   
		            <div class="form-group">
		            	<label for="youtubedescription"><span class="glyphicon glyphicon-copy" aria-hidden="true" id="copybuttonDescription"></span> Youtube Description <span class="badge" id="youtubedescriptionLength" name="youtubedescriptionLength"></span></label>
		            	<textarea type="text" rows="7" class="form-control videopreview" id="youtubedescription" name="youtubedescription">$JIRA_FIELD_YOUTUBE_DESCRIPTION</textarea>
		            	
		            </div>
		            <div class="form-group">
		                <label for="youtubetags"><span class="glyphicon glyphicon-copy" aria-hidden="true" id="copybuttonTags"></span> Youtube Tags <span class="badge" id="youtubetagsLength" name="youtubetagsLength"></span></label>
		                <textarea type="text" rows="7" class="form-control videopreview" id="youtubetags" name="youtubetags">$JIRA_FIELD_YOUTUBE_TAGS</textarea>
		                
		            </div>

				   	<div class="form-group">
		                <div class='datepicker1' id='datetimepicker1'>
		                	<label for="datepicker">Release Date | Last Release: $last_release</label>
		                    <input required="true" type='text' class="form-control" id="release_date" name="release_date" value="$next_release"/>             
		                    <span class="help-inline">$release_days</span>
		                </div>
	            	</div>
	            	<div class="form-group">
		                <input type="submit" type="button" class="btn btn-danger" id="save_jira" name="save_jira" value="SAVE IN JIRA">
	            	</div>

					<script>
					  $( function() {
					    $( "#release_date" ).datepicker();
					  } );
					  </script>
			    
		    </div>        
		</div>
	</div>
	
     <div class="panel panel-default" id="videopreparation">
        
          <!-- PANEL TITLE -->
          <div class="panel-heading">
          	 <h3 class="panel-title prep"><span style="color:green;font-size:12pt;">$JIRA_FIELD_PROJECT_NAME - $JIRA_FIELD_SUMMARY ($JIRA_FIELD_STATUS)</span></h3>
          </div>
        
          <!-- CONTENT -->
          <div class="panel-body">
             <div class="form-group keywords">
                <label for="primarykeyword">1st Keyword <span id="refreshPrimarykeyword" class="glyphicon glyphicon-refresh" aria-hidden="true"></span></label>
                <input value="$JIRA_FIELD_PRIMARY_KEYWORD" type="text" class="form-control" id="primarykeyword" name="primarykeyword">
             </div>
             <div class="form-group keywords">
                <label for="secondarykeyword">2nd Keyword <span id="refreshSecondarykeyword" class="glyphicon glyphicon-refresh" aria-hidden="true"></span></label>
                <input value="$JIRA_FIELD_SECONDARY_KEYWORD" type="text" class="form-control" id="secondarykeyword" name="secondarykeyword">
             </div>  
             <div class="form-group"></div>  
             <div class="form-group">
                <label for="titlesuggestion">Title (Suggestion) <span id="refreshTitlekeyword"  class="glyphicon glyphicon-refresh" aria-hidden="true"></span> |  <span id="maketitle" class="glyphicon glyphicon-star" aria-hidden="true"></span></label>
                 <input required="true" value="$JIRA_FIELD_KEYWORDS_TITLE" type="text" class="form-control" id="titlekeyword" name="titlekeyword">
             </div>
             <div class="form-group">
                <label for="calltoaction">Call To Action <span id="refreshCalltoaction" class="glyphicon glyphicon-refresh" aria-hidden="true"></span></label>
                <input required="true" value="$JIRA_FIELD_CALL_TO_ACTION" type="text" class="form-control" id="calltoaction" name="calltoaction">
             </div>
              <div class="form-group">
                <label for="videolink">Youtube Link <span id="refreshCalltoaction" class="glyphicon glyphicon-refresh" aria-hidden="true"></span></label>
                <input required="true" value="$JIRA_FIELD_FINAL_VIDEO_LINK" type="text" class="form-control" id="videolink" name="videolink">
             </div>
             
              <div class="form-group list">
                <label for="list">List <span id="refreshList" class="glyphicon glyphicon-refresh" aria-hidden="true"></span></label>
                <textarea rows="7" class="form-control" id="list" name="list">$JIRA_FIELD_DESCRIPTION_LIST</textarea>
              </div>
             
             <div class="form-group sources">
                <label for="sources">Sources <span id="refreshSources" class="glyphicon glyphicon-refresh" aria-hidden="true"></span></label>
                <textarea rows="7" class="form-control" name="sources" id="sources">$credit_output</textarea>
              </div>   
              
              <div class="form-group">
                <label for="tags">Tags <span id="refreshTags" class="glyphicon glyphicon-refresh" aria-hidden="true"></span></label>                
                <textarea rows="7" class="form-control" name="tags" id="tags">$JIRA_FIELD_TAGS</textarea>
              </div>
                <div class="form-group list">
                <label for="branding">Branding</label>
               $branding_tags_dropdown
              </div>
              <div class="form-group sources">
              	<button id="preview" type="button" class="btn btn-success updatejira">Preview</button>
              </div>
             </form>  

          </div>
        
     </div> 
     </div> 
EOF;

	return $output;
}

function _formgroupButton($name)
{
	$output = _injectHiddenField($name, $name);
	$output .="<button type='button' onclick='submit();'>$name</button>";
	return $output;
}

function _formgroupSubmitbutton($name)
{
	return "<input type='submit' name='".$name."' id='".$name."' value='$name'></input>";
	
}

function _formgroupCheckbox($name, $value)
{
	return "<div class='checkbox'>
  				<label><input type='checkbox' name='".$name."' id='".$name."'>$value</label>
			</div>";
}

function _formgroupInput($name, $value)
{
	return "<div class='form-group'>
			<label for='".$name."'>".$name."</label>
        	<input type='text' class='form-control' name='".$name."' id='".$name."' value='".$value."'/>                    
        </div>";
}

function _formgroupTextarea($name, $value = false, $col = 3, $row = 3, $class = false, $required = false)
{	$required = ($required !== false) ? "required=true" : "";
	return "<div class='form-group'>
			<label for='".$name."'>".$name."</label>
        	<textarea ".$required."  col='".$col."' row='".$row."' type='text' class='form-control $class' name='".$name."' id='".$name."'>".$value."</textarea>
        </div>";
}
function _injectHiddenField($name, $value)
{
	return "<input type=\"hidden\" id=\"".$name."\" name=\"".$name."\" value=\"".$value."\">";
}
?>