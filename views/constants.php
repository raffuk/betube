<?php
	//SERVERS
$whitelist = array(
    'localhost',
    '127.0.0.1',
    '::1'
);

if(!in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
   	$dbName = 'palac5_betube' ; 
	$dbHost = 'localhost' ;
	$dbUsername = 'palac5_betube';
	$dbUserPassword = 'TNwPMl]!D_^_';
}else{
	$dbName = 'betube' ; 
	$dbHost = 'localhost' ;
	$dbUsername = 'root';
	$dbUserPassword = 'root';
}

define ("DB_BETUBE_NAME", $dbName);
define ("DB_HOST", $dbHost);
define ("DB_USER", $dbUsername);
define ("DB_PASS", $dbUserPassword);
	// JIRA
		// PROJECT TYPES
		define("JIRA_PROJECT_TYPE_VIDEO", "video");
		define("JIRA_PROJECT_TYPE_TASK", "task");
	
		// STATUSES
		define("JIRA_STATUS_BACKLOG", "Backlog");
		define("JIRA_STATUS_PIPELINE", "Pipeline");
		define("JIRA_STATUS_IN_PROGRESS", "In Progress");
		define("JIRA_STATUS_QA", "QA"); // Ready for Review
		define("JIRA_STATUS_QA_AND_READY_FOR_REVIEW", "QA"); // Ready for Review
		define("JIRA_STATUS_READY_FOR_REVIEW", "Ready for Review"); // Ready for Review - AFTER QA, NOT USED AT MOMENTO
		define("JIRA_STATUS_PREPARE_FOR_RELEASE", "Prepare for Release"); 
		define("JIRA_STATUS_READY_FOR_RELEASE", "Ready for Release"); 
		define("JIRA_STATUS_RELEASE", "Release"); // Release
		define("JIRA_STATUS_RELEASED", "Released"); // Released
		define("JIRA_STATUS_RELEASED_LAST_MONTH", "JIRA_STATUS_RELEASED_LAST_MONTH"); // Released
		define("JIRA_STATUS_SCHEDULED_FOR_RELEASE", "JIRA_STATUS_SCHEDULED_FOR_RELEASE"); // Released
		define("JIRA_STATUS_RELEASED_THIS_MONTH", "JIRA_STATUS_RELEASED_THIS_MONTH"); // Released
		
		// FIELDS array("assignee","updated","status"
		define("JIRA_FIELD_SUMMARY", "summary");
		define("JIRA_FIELD_DESCRIPTION", "description");
		define("JIRA_FIELD_ASSIGNEE", "assignee");
		define("JIRA_FIELD_ASSIGNEE_KEY", "key");
		define("JIRA_FIELD_ASSIGNEE_NAME", "name");
		define("JIRA_FIELD_ASSIGNEE_DISPLAYNAME", "displayName");
		define("JIRA_FIELD_ASSIGNEE_AVATARURLS", "avatarUrls");
		define("JIRA_FIELD_ASSIGNEE_AVATAR", "avatar");
		define("JIRA_FIELD_ASSIGNEE_AVATAR_16X16", "16X16");
		define("JIRA_FIELD_ASSIGNEE_AVATAR_24X24", "24X24");
		define("JIRA_FIELD_ASSIGNEE_AVATAR_32X32", "32X32");
		define("JIRA_FIELD_ASSIGNEE_AVATAR_48X48", "\"48X48\"");
		define("JIRA_FIELD_ATTACHMENTS", "");
		define("JIRA_FIELD_UPDATED", "updated");
		define("JIRA_FIELD_STATUS", "status");
		define("JIRA_FIELD_STATUS_NAME", "name");
		define("JIRA_FIELD_PROJECT", "project");
		define("JIRA_FIELD_PROJECT_KEY", "key");
		define("JIRA_FIELD_PROJECT_NAME", "name");
		define("JIRA_FIELD_RELEASE_DATE", "customfield_10038");
		define("JIRA_FIELD_COMMENT", "comment");
		define("JIRA_FIELD_COMMENTS", "comments");
		define("JIRA_FIELD_COMMENTS_TOTAL", "total");
		define("JIRA_FIELD_YOUTUBE_URL", "customfield_10028");
		define("JIRA_FIELD_GOOGLE_URL", "customfield_10027");
		define("JIRA_FIELD_VIDEO_SOURCE", "customfield_10034");
		define("JIRA_FIELD_AUDIO_SOURCE", "customfield_10035");
		define("JIRA_FIELD_DESCRIPTION_LIST", "customfield_11303");
		define("JIRA_FIELD_YOUTUBE_DESCRIPTION", "customfield_11305");
		define("JIRA_FIELD_TAGS", "customfield_11402");
		define("JIRA_FIELD_YOUTUBE_TAGS", "customfield_11403");
		define("JIRA_FIELD_CALL_TO_ACTION", "customfield_11401");
		define("JIRA_FIELD_FINAL_VIDEO_LINK", "customfield_10300");
		define("JIRA_FIELD_VIDEO_OF_THE_MONTH", "customfield_11001");
		define("JIRA_FIELD_VIDEO_OF_THE_MONTH_ON", "Candidate for Video of the Month");
		define("JIRA_FIELD_RESOLUTION", "resolution");

		define("JIRA_FIELD_PRIMARY_KEYWORD", "customfield_11300");
		define("JIRA_FIELD_SECONDARY_KEYWORD", "customfield_11301");
		define("JIRA_FIELD_KEYWORDS_TITLE", "customfield_11302");
		define("JIRA_FIELD_YOUTUBE_METATAG", "customfield_11500");
		define("JIRA_FIELD_KEY", "key");
		
		// QUERIES
		  // IN PROGRESS
		  // READY FOR RELEASE
		  // SCHEDULED (RELEASE DATE > TODAY)
		  // SCHEDULED (UNTIL TODAY 11:59)
		  
		  // READY FOR REVIEW
		  
		define("JIRA_QUERY_GET_BACKLOG", 'status = "'.JIRA_STATUS_BACKLOG.'" AND type = "'.JIRA_PROJECT_TYPE_VIDEO.'"');		
		define("JIRA_QUERY_GET_PIPELINE", 'status = "'.JIRA_STATUS_PIPELINE.'" AND type = "'.JIRA_PROJECT_TYPE_VIDEO.'"');		
		define("JIRA_QUERY_GET_IN_PROGRESS", 'status in ("'.JIRA_STATUS_IN_PROGRESS.'","'.JIRA_STATUS_QA.'","'.JIRA_STATUS_READY_FOR_REVIEW.'") AND type = "'.JIRA_PROJECT_TYPE_VIDEO.'"');		
		define("JIRA_QUERY_GET_READY_FOR_RELEASE", 'status in ("'.JIRA_STATUS_READY_FOR_RELEASE.'") AND type = "'.JIRA_PROJECT_TYPE_VIDEO.'"');		
		define("JIRA_QUERY_GET_READY_FOR_RELEASE_NOT_RELEASED_YET", 'status in ("'.JIRA_STATUS_READY_FOR_RELEASE.'") AND type = "'.JIRA_PROJECT_TYPE_VIDEO.'"');		
		define("JIRA_QUERY_GET_READY_FOR_RELEASE_AND_PREPARE_FOR_RELEASE", 'status in ("'.JIRA_STATUS_READY_FOR_RELEASE.'", "'.JIRA_STATUS_PREPARE_FOR_RELEASE.'") AND type = "'.JIRA_PROJECT_TYPE_VIDEO.'"');		
		define("JIRA_QUERY_GET_SCHEDULED_FOR_RELEASE", 'status in ("'.JIRA_STATUS_RELEASED.'") AND "Release Date">endOfDay(0) AND type = "'.JIRA_PROJECT_TYPE_VIDEO.'"');		
		define("JIRA_QUERY_GET_RELEASED", 'status in ("'.JIRA_STATUS_RELEASED.'") AND "Release Date"<=endOfDay(0) AND "Release Date">=endOfMonth(-5) AND type = "'.JIRA_PROJECT_TYPE_VIDEO.'"');		
		
		define("JIRA_QUERY_GET_READY_FOR_REVIEW", 'status = "'.JIRA_STATUS_READY_FOR_REVIEW.'"');		
		define("JIRA_QUERY_GET_READY_FOR_REVIEW_AND_QA", 'type = '.JIRA_PROJECT_TYPE_VIDEO.' AND status in ("'.JIRA_STATUS_QA.'","'.JIRA_STATUS_READY_FOR_REVIEW.'"'.')');		
		define("JIRA_QUERY_GET_READY_FOR_REVIEW_TODAY", 'status changed to ("'.JIRA_STATUS_QA.'","'.JIRA_STATUS_READY_FOR_REVIEW.'") AFTER startOfDay(0) AND type = "'.JIRA_PROJECT_TYPE_VIDEO.'"');	
		define("JIRA_QUERY_GET_READY_FOR_REVIEW_THIS_WEEK", 'status changed to ("'.JIRA_STATUS_QA.'","'.JIRA_STATUS_READY_FOR_REVIEW.'") AFTER startOfWeek(0) AND type = "'.JIRA_PROJECT_TYPE_VIDEO.'"');	
		define("JIRA_QUERY_GET_READY_FOR_REVIEW_LAST_WEEK", 'status changed to ("'.JIRA_STATUS_QA.'","'.JIRA_STATUS_READY_FOR_REVIEW.'") AFTER startOfWeek(-1) BEFORE startOfWeek(0) AND type = "'.JIRA_PROJECT_TYPE_VIDEO.'"');	
		define("JIRA_QUERY_GET_READY_FOR_REVIEW_THIS_MONTH", 'status changed to ("'.JIRA_STATUS_QA.'","'.JIRA_STATUS_READY_FOR_REVIEW.'") AFTER startOfMonth(0) AND type = "'.JIRA_PROJECT_TYPE_VIDEO.'"');	
		define("JIRA_QUERY_GET_READY_FOR_REVIEW_LAST_MONTH", 'status changed to ("'.JIRA_STATUS_QA.'","'.JIRA_STATUS_READY_FOR_REVIEW.'") AFTER startOfMonth(-1) BEFORE startOfMonth(0) AND type = "'.JIRA_PROJECT_TYPE_VIDEO.'"');	
			
		define("JIRA_QUERY_GET_QA", "status in ('".JIRA_STATUS_QA."')");
		define("JIRA_QUERY_GET_RELEASED_LAST_MONTH", "status in ('".JIRA_STATUS_RELEASED."') AND 'Release Date'>=startOfMonth(-1) AND 'Release Date'<=endOfMonth(-1)");
		define("JIRA_QUERY_GET_RELEASED_THIS_MONTH", "status in ('".JIRA_STATUS_RELEASED."') AND 'Release Date'>=startOfMonth(0) AND 'Release Date'<=endOfMonth(0)");
		define("JIRA_QUERY_GET_READY_FOR_RELEASE_AND_PREPARE_FOR_RELEASE_NOT_RELEASED_YET", 'status in ("'.JIRA_STATUS_READY_FOR_RELEASE.'", "'.JIRA_STATUS_PREPARE_FOR_RELEASE.'") AND type = "'.JIRA_PROJECT_TYPE_VIDEO.'" AND "Release date" is EMPTY');
		
		define("JIRA_TRANSITION_RELEASED", '111');
		define("JIRA_TRANSITION_PREPARE_FOR_RELEASE", '281');
		define("JIRA_TRANSITION_QA_REJECTED", '241');
		define("JIRA_TRANSITION_READY_FOR_REVIEW_REJECTED", '221');
		define("JIRA_TRANSITION_NEEDS_EXECUTIVE_APPROVAL", '361');
		
	// BeTube 
		// ?
		
		// READY FOR REVIEW
		define("SUMMARY", "SUMMARY");
		define("TOTAL", "TOTAL");
		define("COMMENTS", "COMMENTS");
		define("TICKETS", "TICKETS");
		define("READY_FOR_REVIEW", "READY_FOR_REVIEW");
		define("BACKLOG", "BACKLOG");
		define("PIPELINE", "PIPELINE");
		define("READY_FOR_REVIEW_TODAY", "READY_FOR_REVIEW_TODAY");
		define("READY_FOR_REVIEW_THIS_WEEK", "READY_FOR_REVIEW_THIS_WEEK");
		define("READY_FOR_REVIEW_LAST_WEEK", "READY_FOR_REVIEW_LAST_WEEK");
		define("READY_FOR_REVIEW_THIS_MONTH", "READY_FOR_REVIEW_THIS_MONTH");
		define("READY_FOR_REVIEW_LAST_MONTH", "READY_FOR_REVIEW_LAST_MONTH");

		// RELEASES
		define("LAST_RELEASE", "LAST_RELEASE");
		define("NEXT_RELEASE", "NEXT_RELEASE");
		define("RELEASED_LAST", "RELEASED_LAST");
		
		//USERS
			//ACCESS
			define("USER_LAST_ACCESS", "USER_LAST_ACCESS");
			
		// LINKS
			//BOARDS
			define("LINK_JIRA_BROWSE", "https://epicmegacorp.atlassian.net/browse/");	
			define("LINK_BOARDS_IN_QA", "https://epicmegacorp.atlassian.net/secure/RapidBoard.jspa?rapidView=50&useStoredSettings=true");	
			define("LINK_BOARDS_READY_FOR_RELEASE", "https://epicmegacorp.atlassian.net/secure/RapidBoard.jspa?rapidView=31&useStoredSettings=true");	
			
			// BOARDS FILTERED
			define("LINK_BOARDS_RELEASE_EPIC_FAILS", "https://epicmegacorp.atlassian.net/secure/RapidBoard.jspa?rapidView=31&quickFilter=330&quickFilter=331&quickFilter=348");	
						
					
			// FILTERS
			define("LINK_FILTER_IN_QA", "https://epicmegacorp.atlassian.net/issues/?filter=10103");	
			define("LINK_FILTER__READY_FOR_RELEASE", "https://epicmegacorp.atlassian.net/issues/?filter=10030");	
			define("LINK_FILTER__PREPARE_FOR_RELEASE", "https://epicmegacorp.atlassian.net/issues/?filter=12501");	
			define("LINK_FILTER__READY_FOR_RELEASES_AND_PREPARE_FOR_RELEASE", "https://epicmegacorp.atlassian.net/issues/?filter=12500");	

		//TITLES
		define("Ready for Review", "");
		define("Ready for Release", "");
		
		define("TEMPLATE_YOUTUBE_DESCRIPTION", 
			"[title]. [headLight]
Subscribe: [channelLink] | Facebook: [facebookLink]
Submit your video: http://bit.ly/submit-your-vid
Business inquiries/contact: http://bit.ly/ContactEpic 

▾▾▾ More awesome stuff below ▾▾▾
[channelName] prepared for you a video of [firstTitle], check it out!

So there you have it! [firstTitle]. [headLight]
[list]
[credits]
Disclaimer: We try to reach out to all creators to ask for permission to use their videos in our compilations. Did we miss you? PLEASE contact us at info@epicmegacorp.com before filing a copyright claim - we will do our best to come to an agreement!

--------------------- subscribe [channelLink]

[firstTitle]: [videolink]

Subscribe to [channelName] Channel to make sure you catch the absolute best in [bestIn]!

Thanks for watching [channelName]!

More on [channelName]:
[externalLinks]

Epic Megacorp Network:
Adrenaline Channel: http://bit.ly/AdrenalineChannel
Epic Dash Cam Channel: http://bit.ly/EpicDashcams
Epic Fitness Channel: http://bit.ly/EpicFitnessChann
Epic Food Channel: http://bit.ly/EpicFood
Epic Girls Channel: http://bit.ly/EpicGirlsSubscribe
Epic Laughs Channel: http://bit.ly/EpicLaughsChannel
Epic Life Channel: http://bit.ly/EpicLifeChannel
Epic Lists Channel: http://bit.ly/EpicListsSubscribe
Epic Method Channel: http://bit.ly/EpicMethod
Epic Motivation Channel: http://bit.ly/EpicMotivation
Epic Music Channel: http://bit.ly/EpicMusicChannel
Epic Play Channel: http://bit.ly/EpicPlayChannel
Epic Surf Channel: http://bit.ly/EpicSurf
Funny Pets: http://bit.ly/FunnyPetMedia
TNT Channel: http://bit.ly/TNTChannelTV

Trip Burger Network:
Trip Burger Laughs: http://bit.ly/TripBurgerLaughs
Trip Burger Pets: http://bit.ly/TripBurgerPets");

?>