$(document).ready(function(){
	function _checkYoutubeLink()
	{
		if($("input#videolink").val() == "")
			$("input#videolink").css("background-color", "red");
		else
			$("input#videolink").css("background-color", "white");

	}

	function _saveTicketinClipboard(input)
	{
		var copytext = document.querySelector(input);
		copytext.select();
		try 
		{
			var successful = document.execCommand('copy');
			var msg = successful ? 'successful' : 'unsuccessful';
			console.log('Copying text command was ' + msg);
		} 
		catch (err) 
		{
			console.log('Oops, unable to copy');
		}
	}

	_checkYoutubeLink();
	
	$("input#videolink").change(function(){
		_checkYoutubeLink();
	});
	//TAGS LENGTH COUNTER
	
	//DESCRIPTION LENGHT COUNTER
	_showfieldcounter("textarea#youtubetags","#youtubetagsLength",500);
	_showfieldcounter("textarea#youtubedescription","#youtubedescriptionLength",5000);

	// PREPARE Copy to Clipboard buttons
	$("#copybuttonSummary").click(function(){
		_saveTicketinClipboard("input#summary");
	});
	$("#copybuttonDescription").click(function(){
		_saveTicketinClipboard("#youtubedescription");
	});

	$("#copybuttonTags").click(function(){
		_saveTicketinClipboard("#youtubetags");
	});
	
	function _showfieldcounter(field,span,limit = 500){
		$(field).keyup(function(){
			$(span).html($(field).val().length);
			console.log($(field).val() + ": " + $(field).val().length);
			if($(field).val().length > limit)
			{
				$(span).css("color", "red");
				$(span).css("background-color", "gray");
			}
			else
			{
				$(span).css("color", "black");
				$(span).css("background-color", "gray");
			}
		});
		
	}

	$("span#maketitle").click(function(){          
		string = $.trim($("#primarykeyword").val()) + " ★ " + $.trim($("#secondarykeyword").val());
	    $("#titlekeyword").val(string).effect("highlight", {}, 1500);
	});

	var o_primarykeyword = $.trim(document.getElementById("primarykeyword").value);
		$("span#refreshPrimarykeyword").click(function(){                
		$("#primarykeyword").val(o_primarykeyword).effect("highlight", {}, 1500);
	});

	var o_secondarykeyword = $.trim(document.getElementById("secondarykeyword").value);
		$("span#refreshSecondarykeyword").click(function(){
	    $("#secondarykeyword").val(o_secondarykeyword).effect("highlight", {}, 1500);
	});

	var o_titlekeyword = $.trim(document.getElementById("titlekeyword").value);
		$("span#refreshTitlekeyword").click(function(){
	    $("#titlekeyword").val(o_titlekeyword).effect("highlight", {}, 1500);
	});

	var o_calltoaction = $.trim(document.getElementById("calltoaction").value);
		$("span#refreshCalltoaction").click(function(){
	    $("#calltoaction").val(o_calltoaction).effect("highlight", {}, 1500);
	});

	var o_sources = document.getElementById("sources").value;
		o_sources = replaceAll(o_sources, "be.com/watch?v=", ".be/");
		o_sources = replaceAll(o_sources, "\n\n\n\n", "\n\n");
		o_sources = replaceAll(o_sources, "\n\n\n", "\n");
		o_sources = replaceAll(o_sources, "\n\n", "\n");
		o_sources = replaceAll(o_sources, "\nAudio:", "\n\nAudio:");
		


	$("span#refreshSources").click(function(){
	    $("#sources").val(o_sources).effect("highlight", {}, 1500);
	});

	var o_list = $.trim(document.getElementById("list").value);
		$("span#refreshList").click(function(){
	    $("#list").val(o_list).effect("highlight", {}, 1500);
	});

	var o_tags = $.trim(document.getElementById("tags").value);
		$("span#refreshTags").click(function(){
	    $("#tags").val(o_tags).effect("highlight", {}, 1500);
	});

	

	$("button#preview").click(function(){
		$("input#summary").val($("input#titlekeyword").val() + " [" + $("input#hidden_name").val() + "]").effect("highlight", {}, 1500);		
		$("textarea#youtubedescription").val(prepare_description()).effect("highlight", {}, 1500);;
		compare();

		var youtubedescription = $("textarea#youtubedescription").val();
		$("span#youtubedescriptionLength").text(youtubedescription.length);
		
		if(youtubedescription.length>=5000)
			$("span#youtubedescriptionLength").css('background-color', 'red');
		else
			$("span#youtubedescriptionLength").css('background-color', 'blue');
	});	

	$("span#refreshSources").click();

		// $(".branding_tags_dropdown").change(function(){
		// 	console.log($("select#branding").val());
		// 	//$("#tags").val($("#tags").val() + $("#branding").val());
		// });












});



function prepare_description()
{
	var template = $("input#hidden_template").val();
	var externallinks = $("input#hidden_links").val();
	var brand = " [" + $("input#hidden_name").val() + "]";

	externallinks = replaceAll(externallinks, "	/watch?v=", ": http://youtu.be/");
	externallinks = replaceAll(externallinks, brand, "");
	externallinks = replaceAll(externallinks, "Link	URL\n", "");

	template = replaceAll(template, "[title]", $("input#titlekeyword").val());
	template = replaceAll(template, "[firstTitle]", $("input#primarykeyword").val());
	template = replaceAll(template, "[secondTitle]", $("input#secondarykeyword").val());
	template = replaceAll(template, "[headLight]", $("input#calltoaction").val());
	template = replaceAll(template, "[list]", $("textarea#list").val());
	template = replaceAll(template, "[videolink]", $("input#videolink").val());
	template = replaceAll(template, "[credits]", $("textarea#sources").val());
	template = replaceAll(template, "[channelName]", $("input#hidden_name").val());
	template = replaceAll(template, "[bestIn]", $("input#hidden_bestin").val());
	template = replaceAll(template, "[channelLink]", $("input#hidden_channel_link").val());
	template = replaceAll(template, "[facebookLink]", $("input#hidden_facebook_link").val());
	
	template = replaceAll(template, "[externalLinks]", externallinks);
	
	return template;
}
function prepare_tags()
{
	var tags = $("input#primarykeyword").val();
	
	tags = tags + $("input#primarykeyword").val();
	

}

function compare(){
	var
		tags1 = $("textarea#tags").val();
		tags2 = $("select#branding").val();

	tags1 = $.trim(tags1);
	tags1 = tags1.split(',');
	tags2 = $.trim(tags2);
	tags2 = tags2.split(",");
	
		
		differences = [];
		all = [];
	
	for(tag of tags1){
	
		tag = tag.toLowerCase();
		if (tags2.indexOf(tag.trim()) == -1 && tag !== ""){
			differences.push(tag)					
		}
	}

	for(tag of tags2){
	
		tag = tag.toLowerCase();
	
		if (tags1.indexOf(tag.trim()) == -1 && tag !== ""){
			differences.push(tag)
		} 
	}
	//console.log(differences);
	differences = uniq(differences);	
	if (differences.length > 0) {
		var alltags = differences.join(",")
		$("textarea#youtubetags").val(differences).effect("highlight", {}, 1500);
		$("span#youtubetagsLength").html(alltags.length+(differences.length) + "/500");
		


		if(alltags.length>=500)
			$("span#youtubetagsLength").css('background-color', 'red');
		else
			$("span#youtubetagsLength").css('background-color', 'blue');
		
	}
	
	//console.log(differences.length)
	//console.log(alltags.length+(differences.length))
}

function uniq(a) {
    var prims = {"boolean":{}, "number":{}, "string":{}}, objs = [];

    return a.filter(function(item) {
        var type = typeof item;
        if(type in prims)
            return prims[type].hasOwnProperty(item) ? false : (prims[type][item] = true);
        else
            return objs.indexOf(item) >= 0 ? false : objs.push(item);
    });
}
function wait(miliseconds)
{
	setTimeout(function () {

}, 1000);
}		

function nospaces(a) {
	
	return $.trim(a);
}
function replaceAll(str, find, replace) {
	return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function escapeRegExp(str) {
	return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}


function _copy_all_video_data()
	{
		$("a#copybuttonSummary").click();
		
		$("a#copybuttonDescription").click();
		
		$("a#copybuttonTags").click();
		
	}


var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;

function onYouTubeIframeAPIReady() {
    player = new YT.Player('youtube_preview', {
        events: {
            'onReady': onPlayerReady
        }
    });
}

function onPlayerReady(event) {
    event.target.playVideo();
}


function ShowTime() {

    alert(player.getCurrentTime());
} 
	