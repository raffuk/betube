<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
  <?php

//pull in login credentials and CURL access function
require_once("jira_php/utils.php");
//call all the functions 
require_once("functions.php");

class dashboard
{
	function __construct()
	{
		echo "Starting Dashboard...<br/>";
	}

	function __destruct()
	{
		echo "Destroying Dashboard...";
	}
}

class workload extends dashboard
{
	function __construct()
	{
		echo "workload starting.";
	}

	function __destruct()
	{
		echo "workload finishing.";
	}
}

class tickets extends workload
{	
	protected $issue;
	protected $result;
	
	function __construct()
	{
		echo "Tickets starting.";
	}

	function get($jql, $fields = false)
	{
		$data = array('jql' => $jql,
					'fields' => $fields,
					'maxResults' => 2000);

		return get_from('search', $data);
	}

	function __destruct()
	{
		echo "Tickets finishing.";
	}
}


$tickets = new tickets;
$ticketsInprogress = $tickets->get('status = "In Progress"', array("assignee"));

//p($ticketsInprogress);
$ticket = array();
$i = 0;

$workload = array();
$assignee = "";

foreach ($ticketsInprogress->issues as $ticketKey => $value) {
	$assignee = (isset($value->fields->assignee->key)) ? $value->fields->assignee->key : "Annonymous"; 
	
	$workload[$assignee]["In Progress"] ++;
}

$ticketsReadyforreviewToday = $tickets->get('status changed to ("QA") AFTER startOfDay(0)', array("assignee"));

foreach ($ticketsReadyforreviewToday->issues as $key => $value) {
	$workload[$value->fields->assignee->key]["Today"] ++;	
}

$ticketsReadyforreviewThisweek = $tickets->get('status changed to ("QA") AFTER startOfWeek(0)', array("assignee"));

foreach ($ticketsReadyforreviewThisweek->issues as $key => $value) {
	$workload[$value->fields->assignee->key]["thisWeek"] ++;
}

$ticketsReadyforreviewLastweek = $tickets->get('status changed to ("QA") AFTER startOfWeek(-1) BEFORE startOfWeek(0)', array("assignee"));

foreach ($ticketsReadyforreviewLastweek->issues as $key => $value) {
	$workload[$value->fields->assignee->key]["lastWeek"] ++;
}

$ticketsReadyforreviewThismonth = $tickets->get('status changed to ("QA") AFTER startOfMonth(0)', array("assignee"));

foreach ($ticketsReadyforreviewThismonth->issues as $key => $value) {
	$workload[$value->fields->assignee->key]["thisMonth"] ++;
}

$ticketsReadyforreviewLastmonth = $tickets->get('status changed to ("QA") AFTER startOfMonth(-1) BEFORE startOfMonth(0)', array("assignee"));

foreach ($ticketsReadyforreviewLastmonth->issues as $key => $value) {
	$workload[$value->fields->assignee->key]["lastMonth"] ++;
}

//p($workload);

?>

  <body>

    <div class="container">
		<div class="page-header">
			<h1>BeTube 0.2</h1>
		</div>
		<?php print createPerformancetable($workload); ?>
	</div>
   
  </body>
</html>

