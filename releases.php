<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>BeTube - Dashboard</title>

    <!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.min.css" rel="stylesheet">
    
	<style>
	#in_progress_assignees {
    display: none;
}
 	.alert-warning{
		font-size:11px;
		text-align:left;
		padding: 5px;
	}
	body{
 		font-size:12px;
	}

  #panel{
    display:block;
  }
  
  #videopreparation{
    float:left;
    width: 50%;
  }

  #videopreview{
    float:right;
    width: 50%;
  }

  .container{
    width: 100%;
    padding: 5px;

  }

  .videoprep{
    float:left;
    width: 50%;
  }
  .videopreview{
    float:right;
    /*width: 50%;*/
  }
  
  #youtube_preview {
    min-height: 20%;
  }
.keywords {
    width: 50%;
    float: left;
}
#save_jira {
  width: 100%;
  height: 60px;
}
.updatejira {
    float: right;
    display: block;
    max-width: 50%;
    height: 60px;
}
.panel-title .prep{
  float:left;
}

.sources{
  float:right;
  width: 50%;
  display: block;
}
.sources button{
  width: 100%;
}

.list select{
  width: 100%;
}

.list{
  float:left;
  width: 50%;
  display: block;
}
textarea, input, select{
  font-size:11px !important;
}
button {
  font-size:12pt !important;
}
.glyphicon {
  
  cursor: pointer;
}
select .selectpicker{
  width: 100%;
  display: block;
}


.form-group .youtube{
  display: block;
  float:left;
  width: auto;
  height: auto;

}
.form-group .images {
    display: block;
    float: right;
}

.carousel-inner>.item>a>img, .carousel-inner>.item>img {
        display: block;
        width:290px;
        height:150px;
        max-width: auto;
        max-height: auto;
}

.visuals{
        display: block;
        max-width:650px;
        max-height:155px;
        width: auto;
        height: 155px;
        padding:3;
}
.avatar{
    max-width: 24px;
    max-height: 24px;
    margin-top: -8px;
    margin-bottom: -6px;
    margin-left: -5px;
    }


	</style>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="clipboard/clipboard.min.js"></script>
  <script src="iframe_api.js"></script>
  <script type="text/javascript" src="prepare.js"></script>
  </head>
<?php
require_once("constants.php");
//pull in login credentials and CURL access function
require_once("jira_php/utils.php");
//call all the functions 
require_once("functions.php");
require_once("classes.php");

$releases = new Releases();


//$slack = new Slack();


?>
  <body>

    <div class="container">
      <div id="panel">
        
        <?php 
        
        echo createReleaseScreen($releases);

        ?>
          
      </div>
    </div>  
 
  </body>
</html>