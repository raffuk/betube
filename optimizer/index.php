<!doctype html>
<html lang="us">
<head>
	<script src="../jquery-3.1.1.min.js"></script>
	<script src="../stopwatch.js"></script>

	<meta charset="utf-8">
	<title>BETUBE 0.01</title>
	<link href="../jquery-ui/jquery-ui.css" rel="stylesheet">
	<style>
	
	body{
		font-family: "Trebuchet MS", sans-serif;
	}

	.demoHeaders {
		margin-top: 2em;
	}

	#dialog-link {
		padding: .4em 1em .4em 20px;
		text-decoration: none;
		position: relative;
	}

	#dialog-link span.ui-icon {
		margin: 0 5px 0 0;
		position: absolute;
		left: .2em;
		top: 50%;
		margin-top: -8px;
	}

	#icons {
		margin: 0;
		padding: 0;
	}

	#icons li {
		margin: 2px;
		position: relative;
		padding: 4px 0;
		cursor: pointer;
		float: left;
		list-style: none;
	}

	#icons span.ui-icon {
		float: left;
		margin: 0 4px;
	}

	.fakewindowcontain .ui-widget-overlay {
		position: absolute;
	}
	
	#title {
		
		display: inline-block;

	}
	#title input { 
		width:100%;

	}
	button{
		height: 27vh;
		width:100%;
	}
	textarea{
		height: 200px;
		width: 100%;

	}

	.sidebyside{
		display: table;
		table-layout: fixed;
		width:100%;			

	}
	.sidebyside div{
		display: table-cell;
	}
	#release {
		display: table;
	}
	#description {
		display: table-cell;
	    width: 50%;
	    float: left;

	}
	#releaseDays {
		display: block;
		font-size: 14px;
		padding-bottom: 5px;
	}
	#channelName {
		font-size:28px;
	}
	#tags {
		display: table-cell;
	    width: 50%;
	    float: right;
	
	}
	
	#tags div {
		width: 100%;
	}
	
	#tags fieldset div textarea {
		height: 122px;
	}
	
	#description fieldset div textarea {
		height: 140px;
	}

	#tagsLength {
		

	}
	
	input { 
		width: 100%;
	}

	.sidebyside input { 
		width: 95%;
	}

	label {
		font-size: 14px;
	}
	select {
		width: 200px;
	}

	body {
	    display: block;
	    margin: 8px;
	    height: 100vh;
	}

	label#reload {
		cursor: pointer;
	}
	span#time{
		font-size: 12px;
	}
	
		
	</style>
</head>
<body>
	<!-- CHOOSE CHANNEL -->
	<?php
		$selectedChannel = (isset($_POST['selectedChannel'])) ? $_POST['selectedChannel'] : "Adrenaline Channel";

		$channelList = array(
						"Adrenaline Channel",
						"Epic Dash Cam",
						"Epic Fails",
						"Epic Fitness Motivation",
						"Epic Food",
						"Epic Girls",
						"Epic Laughs",
						"Epic Life",
						"Epic Lists",
						"Epic Method",
						"Epic Motivation",
						"Epic Music",
						"Epic Play",
						"Epic Surf",
						"Fun with Animals",
						"Funny Pets",
						"TNT Channel",
						"Trip Burger Laughs",
						"Trip Burger Pets"
			);
	?>
	<div id="channelInfo"><span id="channelName"></span>
		

		<form action="index.php" method="POST">
			<select name="selectedChannel" id="selectedChannel" onchange="submit();">
				<?php foreach($channelList AS $key => $value) { ?>
					<option value="<?php echo $value; ?>" <?php if($selectedChannel == $value) echo 'selected'; ?>><?php echo $value; ?></option>
				<?php } ?>
			</select><label id="reload" onclick="location.reload();"> ♻︎ </label><span id="time"></span>
		</form>
		<div id="releaseDays"></div>
	</div>
	<div id="release">
		<div id="description">
			<fieldset class="title">
			<!-- Title -->				
				<div class="sidebyside">
					<div>
						<input value="" name="firstTitle" id="firstTitle" autofocus></input>
					</div>
					<div>
						<input value=""name="secondTitle" id="secondTitle"></input>
					</div>
				</div>
				<div class="">
					<div class="">
						<label>Video Link</label>
						<input name="videoLink" id="videoLink"></input>
					</div>				
					<div class="">
						<label>Suggestion</label>
						<input name="suggestionTitle" id="suggestionTitle"></input>
					</div>
				</div>

			</fieldset>
			<fieldset id="title">
				<div class="sidebyside">
					<div>
						<label>List</label>
						<textarea name="list" id="list"></textarea>
					</div>
					<div>
						<label>Credits</label>
						<textarea name="credits" id="credits"></textarea>
					</div>
				</div>
				<div>
					<label>Preview</label>
					<textarea name="preview" id="preview"></textarea>
					<label id="descriptionLength">0/5000</label>
				</div>
				<button name="" id="copy" onclick="process()">COPY</button>
			</fieldset>	
		</div>
	
		<div id="tags">
			<fieldset class="tags">
				<div>
					<label>Keyword Tags</label>
					<textarea name="" id="original" cols="30" rows="10" onclick="compare()"></textarea>
				</div>
				<div>
					<label>Ending Tags</label>	
					<textarea name="" id="difference" cols="30" rows="10" onclick="compare()"></textarea>
				</div>	
				<div>
					<label>Preview</label>
					<textarea name="" id="differences" cols="30" rows="10" class="js-copytextarea" onclick="compare()"></textarea>
					<label id="tagsLength">0/500</label>
				</div>
				<button onclick="copy();">COPY</button>
			</fieldset>
		</div>
	</div>
  	<script type="text/javascript" src="channelSettings.js"></script> 
  	<script>

  		show();
  		start();
		var e = document.getElementById("selectedChannel");
		var channel = e.options[e.selectedIndex].text;

		var channelFile = "Template.txt";
		var	externalLinkFile = channel + " Links.txt";
		
		function process(){
		 
	 	$(document).ready(function(){
	 		$("#difference").val(channelSettings[channel].tags)	;
	 	});
		 
		 function escapeRegExp(str) {
			return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
			}

		 function replaceAll(str, find, replace) {
			return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
			}
		 
		 var externalLinks;
		
		$.ajax({
			async: false,
			type: 'GET',
			url: externalLinkFile,
			success: function(data){
				data = replaceAll(data, channelSettings[channel].brand, "");
				data = replaceAll(data, "(HD)", "");
				data = replaceAll(data, "  ", "");
				data = replaceAll(data, " :", ":");
				externalLinks = replaceAll(data, "	/watch?v=", ": http://youtu.be/");
			} 
		});		
		 
		// Make Description
		 jQuery.get(channelFile, function(data) {

			var credits = document.getElementById("credits").value.replace(/^\s*[\r\n]/gm, "");
			
			var list = document.getElementById("list").value; 
			
			var secondTitle = document.getElementById("secondTitle").value;

			if(list !="")
				list = list + "\n";
			
			if(credits !="")
			{
				credits = credits + "\n";
				credits = "Credits\n" + credits;
			}

			data = replaceAll(data, "[title]", clearTitle);
			data = replaceAll(data, "[firstTitle]", document.getElementById("firstTitle").value);
			data = replaceAll(data, "[secondTitle]", document.getElementById("secondTitle").value);
			data = replaceAll(data, "[list]", list);
			data = replaceAll(data, "[videolink]", getYoutubeUrl(document.getElementById("videoLink").value));
			data = replaceAll(data, "[credits]", credits);
			data = replaceAll(data, "[channelName]", channel);
			data = replaceAll(data, "[bestIn]", channelSettings[channel].bestIn);
			data = replaceAll(data, "[channelLink]", channelSettings[channel].channelLink);
			data = replaceAll(data, "[facebookLink]", channelSettings[channel].facebookLink);
			data = replaceAll(data, "[externalLinks]", externalLinks);
			data = replaceAll(data, "[headLight]", channelSettings[channel].headLight);

		    $(document).ready(function(){
		        $("#preview").val("");
		        $("#preview").val(data);
		        
		        $("#original").text("")
		        $("#original").append($("#firstTitle").val());
		        $("#original").append(",");
		        $("#original").append($("#secondTitle").val());

		        var value = $("#preview").val();
   		 			
   		 		$("#descriptionLength").text(value.length + "/5000") ;

   		 		if (value.length>5000)
   		 			$("#descriptionLength").css('color','red')
   		 		else
   		 			$("#descriptionLength").css('color','blue')
   		 	});

   		 	
		});
		}

		function getYoutubeUrl(url) {
			var videoid = url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
			if(videoid != null) {
			   return "http://youtu.be/" + videoid[1];
			} else { 
			    console.log("The youtube url is not valid.");
			}
		}

		function normalizeExternalLinks(url) {
			var youtubeLinks = url.match("	/watch?v=");
			console.log(url);
			console.log(youtubeLinks);
			alert(youtubeLinks);
			if(youtubeLinks != null) {
			   return ": http://youtu.be/" + videoid[1];
			} else { 
			    console.log("The youtube url is not valid.");
			}
		}
		
		var clearTitle = ""
		
		$(document).ready(function(){
			//alert(channel + " " + channelSettings[channel].releaseDays);
		    $("#releaseDays").text("Releases: " + channelSettings[channel].releaseDays);
		    $("#channelName").text(channel);


		    $("fieldset.title").click(function(){
		     	    clearTitle = $("#firstTitle").val() + " ★ " + $("#secondTitle").val();
		       	    $("#suggestionTitle").val(clearTitle + " " + channelSettings[channel].brand);  
		    });
		    

		    $("fieldset.tags").click(function(){
		    	compare();
		    });

		    $("[id^='release']").dblclick(function() {
			    process()
			});

   		 });



		function uniq(a) {
		    var prims = {"boolean":{}, "number":{}, "string":{}}, objs = [];

		    return a.filter(function(item) {
		        var type = typeof item;
		        if(type in prims)
		            return prims[type].hasOwnProperty(item) ? false : (prims[type][item] = true);
		        else
		            return objs.indexOf(item) >= 0 ? false : objs.push(item);
		    });
		}
		
		function nospaces(a) {
			return a.map(Function.prototype.call.bind(String.prototype.trim))
		}

		function compare(){
			var
				tags1 = nospaces(document.getElementById("original").value.replace(/, /,",").split(","))
				tags2 = nospaces(document.getElementById("difference").value.replace(/, /,",").split(","))
				differences = [];
				all = [];
			console.log(tags1);
console.log(tags2);
			for(tag of tags1){
			
				tag = tag.toLowerCase();
			
				if (tags2.indexOf(tag.trim()) == -1 && tag !== ""){
					differences.push(tag)					
				}
			}

			for(tag of tags2){
			
				tag = tag.toLowerCase();
			
				if (tags1.indexOf(tag.trim()) == -1 && tag !== ""){
					differences.push(tag)
				} 
			}
			
			differences = uniq(differences);
			console.log(differences);
			if (differences.length > 0) {
				var alltags = differences.join(",")
				document.getElementById("differences").innerText = alltags;
				document.getElementById("tagsLength").innerText = alltags.length+(differences.length) + "/500";
				
				if(alltags.length>500)
					$("#tagsLength").css('color', 'red');
				else
					$("#tagsLength").css('color', 'blue');
				
			}
			
			//console.log(differences.length)
			//console.log(alltags.length+(differences.length))
		}

		function copy() {
			  var copyTextarea = document.querySelector('.js-copytextarea');
			  copyTextarea.select();

			  try {
			    var successful = document.execCommand('copy');
			    var msg = successful ? 'successful' : 'unsuccessful';
			    console.log('Copying text command was ' + msg);
			  } catch (err) {
			    console.log('Oops, unable to copy');
			  }
		}
		
		var wage = document.getElementById("original");
			wage.addEventListener("keydown", function (e) {
		        compare();
		});

		var wage = document.getElementById("difference");
			wage.addEventListener("keydown", function (e) {
			        compare();
		});	

  </script>

</body>
</html>