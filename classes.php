<?php
date_default_timezone_set("America/Sao_Paulo");
Class Dashboard
{
	public $data = array();
	
	function _load($jql, $fields = false, $resource = 'search')
	{
		$data = array('jql' => $jql,
					  'fields' => $fields,
					  'maxResults' => 2000
					  );

		$this->data = get_from($resource, $data);
		//p($this->data,$jql);

	}

	public static function change_status_in_jira($key,$transition,$comment = false)
	{

		$fields = array(
			'transition' => array("id" => "$transition")
		);
		
		if($comment)
			$fields['update'] = array("comment" => array(array("add" => array("body" => "$comment"))));
		
		
		$result = post_to('issue/'.$key."/transitions?expand=transitions.fields", $fields);
		
		if ($result != NULL) {
			$return = "<pre>Error(s) editing issue: ".var_dump($result)."</pre>";
		} else {
			$return = "Edits complete. Issue can be viewed at " . JIRA_URL ."/browse/{$key}\n";
		}

		//p($return);
	}

	public static function save_in_jira($key,$fields = array())
	{

		$fields = array(
			'fields' => $fields
		);
		
		$result = put_to('issue/'.$key, $fields);
		
		if ($result != NULL) {
			return p($result,"Error(s) editing issue:");
		} else {
			return "Edits complete. Issue can be viewed at " . JIRA_URL ."/browse/{$key}\n";
		}

	}

	function save_jira_comment($key,$comment)
	{
		$fields = array(
			'body' => $comment
		);
		
		$result = put_to('issue/'.$key."/comment", $fields);
		
		if ($result != NULL) {
			return p($result,"Error(s) editing issue:");
		} else {
			return "Edits complete. Issue can be viewed at " . JIRA_URL ."/browse/{$key}\n";
		}

	}



	function _store($data,$field)
	{

		foreach ($data->issues as $key => $value) {
			$this->history[$value->fields->assignee->key][$field][TOTAL]++;
			$this->history[$value->fields->assignee->key][$field][TICKETS][] = $value->key;
		}
	}	
	

	function get_projects()
	{
		$this->_load("", array(), 'projects');		
	//	p($this->data);
	}


}

Class Editor extends Dashboard
{
	
	public $history = array();
	public $readyforreview = 0;
	
	function __construct()
	{
	
		$this->get_in_progress();
		$this->get_today();
		$this->get_this_week();
		$this->get_Last_week();
		$this->get_this_month();
		$this->get_last_month();
		$this->get_ready_for_review();
		$this->get_backlog();
		$this->get_pipeline();

		
	}

		function get_in_progress()
		{
			$last_update = array();	
			
			$this->_load(JIRA_QUERY_GET_IN_PROGRESS, array(JIRA_FIELD_ASSIGNEE,JIRA_FIELD_UPDATED,JIRA_FIELD_STATUS));

			foreach ($this->data->issues as $key => $value) {
				
				$last_update[$value->fields->assignee->key][] = $value->fields->updated;
		
				if($value->fields->status->name == JIRA_STATUS_IN_PROGRESS)
					$this->history[$value->fields->assignee->key][JIRA_STATUS_IN_PROGRESS][TOTAL]++;
		

				foreach ($last_update as $ukey => $uvalue) 
				{
					foreach ($uvalue as $eachvalue) {
						if($eachvalue > $last_update_date[$ukey])
							$last_update_date[$ukey] = $eachvalue;
						
						$this->history[$value->fields->assignee->key][JIRA_STATUS_IN_PROGRESS][USER_LAST_ACCESS] = $last_update_date[$ukey];
					}
				}
			}


			
		}
		
		function get_today()
		{
			$this->_load(JIRA_QUERY_GET_READY_FOR_REVIEW_TODAY, array(JIRA_FIELD_ASSIGNEE,JIRA_FIELD_UPDATED,JIRA_FIELD_STATUS));
			
			foreach ($this->data->issues as $key => $value) {
				
				if($value->fields->status->name == JIRA_STATUS_QA) 
				{
					$this->history[$value->fields->assignee->key][READY_FOR_REVIEW_TODAY][TOTAL]++;
					$this->history[$value->fields->assignee->key][READY_FOR_REVIEW_TODAY][TICKETS][] = $value->key;
					
				}
			}
		
		}

		function get_this_week()
		{
			$this->_load(JIRA_QUERY_GET_READY_FOR_REVIEW_THIS_WEEK, array(JIRA_FIELD_ASSIGNEE,JIRA_FIELD_UPDATED,JIRA_FIELD_STATUS));
			
			$this->_store($this->data,READY_FOR_REVIEW_THIS_WEEK);
		}

		function get_pipeline()
		{
			$this->_load(JIRA_QUERY_GET_PIPELINE, array(JIRA_FIELD_ASSIGNEE,JIRA_FIELD_UPDATED,JIRA_FIELD_STATUS));
			
			$this->_store($this->data,JIRA_STATUS_PIPELINE);
		}

		function get_backlog()
		{
			$this->_load(JIRA_QUERY_GET_BACKLOG, array(JIRA_FIELD_ASSIGNEE,JIRA_FIELD_UPDATED,JIRA_FIELD_STATUS));
			
			$this->_store($this->data,JIRA_STATUS_BACKLOG);
		}

		function get_last_week()
		{
			$this->_load(JIRA_QUERY_GET_READY_FOR_REVIEW_LAST_WEEK, array(JIRA_FIELD_ASSIGNEE,JIRA_FIELD_UPDATED,JIRA_FIELD_STATUS));
			
			$this->_store($this->data,READY_FOR_REVIEW_LAST_WEEK);
	
		}

		function get_this_month()
		{
			$this->_load(JIRA_QUERY_GET_READY_FOR_REVIEW_THIS_MONTH, array(JIRA_FIELD_ASSIGNEE,JIRA_FIELD_UPDATED,JIRA_FIELD_STATUS));
			
			$this->_store($this->data,READY_FOR_REVIEW_THIS_MONTH);
		}

		function get_last_month()
		{
			$this->_load(JIRA_QUERY_GET_READY_FOR_REVIEW_LAST_MONTH, array(JIRA_FIELD_ASSIGNEE,JIRA_FIELD_UPDATED,JIRA_FIELD_STATUS));
			
			$this->_store($this->data,READY_FOR_REVIEW_LAST_MONTH);
		}
		
		function get_ready_for_review()
		{
			$this->_load(JIRA_QUERY_GET_READY_FOR_REVIEW, array(JIRA_FIELD_ASSIGNEE,JIRA_FIELD_UPDATED,JIRA_FIELD_STATUS));
			
			$this->history[READY_FOR_REVIEW] = $this->data->issues;
		}

		
}

Class Channel extends Dashboard
{
	public $releases = array();
	// $releases[channel_name]["ready_for_release"]["total"]
	// $releases[channel_name]["ready_for_release"]["tickets"]
	// $releases[channel_name]["scheduled"]["total"]
	// $releases[channel_name]["scheduled"]["tickets"]
	// $releases[channel_name]["last_release"]
	// $releases[channel_name]["recent_release"]

	function __construct()
	{	
		$this->get_in_progress();
 		$this->get_qa();
// 		$this->get_backlog();
	
		$this->get_ready_for_release();
		$this->get_lastmonth();
		$this->get_thismonth();
		$this->get_scheduled();
		$this->get_released();
		$this->get_pipeline();
		$this->get_backlog();
//		$this->get_last_release();
//		$this->get_recent_release();
	}

	function get_in_progress()
	{
		$this->_load(JIRA_QUERY_GET_IN_PROGRESS, array(JIRA_FIELD_SUMMARY,JIRA_FIELD_ASSIGNEE, JIRA_FIELD_UPDATED, JIRA_FIELD_STATUS, JIRA_FIELD_PROJECT, JIRA_FIELD_RELEASE_DATE));

		// Go on each issue, count it and divide into channel, status, totals and tickets
		if(isset($this->data->issues))
			foreach ($this->data->issues as $key => $value) 
			{
				$this->history[$value->fields->project->key][$value->fields->status->name][TOTAL]++;
				$this->history[$value->fields->project->key][$value->fields->status->name][TICKETS][$value->key] = array($value->fields->assignee->name => $value->fields->summary);
			}

	}

	function get_pipeline()
	{
		$this->_load(JIRA_QUERY_GET_PIPELINE, array(JIRA_FIELD_SUMMARY,JIRA_FIELD_ASSIGNEE, JIRA_FIELD_UPDATED, JIRA_FIELD_STATUS, JIRA_FIELD_PROJECT, JIRA_FIELD_RELEASE_DATE));

		// Go on each issue, count it and divide into channel, status, totals and tickets
		if(isset($this->data->issues))
			foreach ($this->data->issues as $key => $value) 
			{
				$this->history[$value->fields->project->key][$value->fields->status->name][TOTAL]++;
				$this->history[$value->fields->project->key][$value->fields->status->name][TICKETS][$value->key] = array($value->fields->assignee->name => $value->fields->summary);
			}

	}

	function get_backlog()
	{
		$this->_load(JIRA_QUERY_GET_BACKLOG, array(JIRA_FIELD_SUMMARY,JIRA_FIELD_ASSIGNEE, JIRA_FIELD_UPDATED, JIRA_FIELD_STATUS, JIRA_FIELD_PROJECT, JIRA_FIELD_RELEASE_DATE));

		// Go on each issue, count it and divide into channel, status, totals and tickets
		if(isset($this->data->issues))
			foreach ($this->data->issues as $key => $value) 
			{
				$this->history[$value->fields->project->key][$value->fields->status->name][TOTAL]++;
				$this->history[$value->fields->project->key][$value->fields->status->name][TICKETS][$value->key] = array($value->fields->assignee->name => $value->fields->summary);
			}

	}

	function get_QA()
	{
		$this->_load(JIRA_QUERY_GET_QA, array(JIRA_FIELD_SUMMARY,JIRA_FIELD_ASSIGNEE, JIRA_FIELD_UPDATED, JIRA_FIELD_STATUS, JIRA_FIELD_PROJECT, JIRA_FIELD_RELEASE_DATE));
			$this->history[$value->fields->project->key][$value->fields->status->name][TOTAL] = 0;
		// Go on each issue, count it and divide into channel, status, totals and tickets
		if(isset($this->data->issues))
		foreach ($this->data->issues as $key => $value) 
		{

			$this->history[$value->fields->project->key][$value->fields->status->name][TOTAL]++;
			$this->history[$value->fields->project->key][$value->fields->status->name][TICKETS][$value->key] = array($value->fields->assignee->name => $value->fields->summary);
		}
		
	}
	function get_ready_for_release()
	{	
		$this->_load(JIRA_QUERY_GET_READY_FOR_RELEASE_AND_PREPARE_FOR_RELEASE, array(JIRA_FIELD_ASSIGNEE, JIRA_FIELD_UPDATED, JIRA_FIELD_STATUS, JIRA_FIELD_PROJECT, JIRA_FIELD_RELEASE_DATE));

		// Go on each issue, count it and divide into channel, status, totals and tickets


		foreach ($this->data->issues as $key => $value) 
		{

			$this->history[$value->fields->project->key][$value->fields->status->name][TOTAL]++;
			$this->history[$value->fields->project->key][$value->fields->status->name][TICKETS][$value->key] = $value->fields->assignee->key;
		}

	}
	
	function get_lastmonth()
	{	
		$this->_load(JIRA_QUERY_GET_RELEASED_LAST_MONTH, array(JIRA_FIELD_SUMMARY,JIRA_FIELD_ASSIGNEE, JIRA_FIELD_UPDATED, JIRA_FIELD_STATUS, JIRA_FIELD_PROJECT, JIRA_FIELD_RELEASE_DATE));

		// Go on each issue, count it and divide into channel, status, totals and tickets
		if(isset($this->data->issues))
			foreach ($this->data->issues as $key => $value) 
			{
				$this->history[$value->fields->project->key][JIRA_STATUS_RELEASED_LAST_MONTH][TOTAL]++;
				$this->history[$value->fields->project->key][JIRA_STATUS_RELEASED_LAST_MONTH][TICKETS][$value->key] = array($value->fields->assignee->name => $value->fields->summary);
		
			}		
	}	
	
	function get_thismonth()
	{

		$this->_load(JIRA_QUERY_GET_RELEASED_THIS_MONTH, array(JIRA_FIELD_SUMMARY,JIRA_FIELD_ASSIGNEE, JIRA_FIELD_UPDATED, JIRA_FIELD_STATUS, JIRA_FIELD_PROJECT, JIRA_FIELD_RELEASE_DATE));

		// Go on each issue, count it and divide into channel, status, totals and tickets
		if(isset($this->data->issues))
			foreach ($this->data->issues as $key => $value) 
			{
				$this->history[$value->fields->project->key][JIRA_STATUS_RELEASED_THIS_MONTH][TOTAL]++;
				$this->history[$value->fields->project->key][JIRA_STATUS_RELEASED_THIS_MONTH][TICKETS][$value->key] = array($value->fields->assignee->name => $value->fields->summary);
			}		
	}
	
	function get_scheduled()
	{
	
	
	//---------
	$this->_load(JIRA_QUERY_GET_SCHEDULED_FOR_RELEASE, array(JIRA_FIELD_SUMMARY,JIRA_FIELD_ASSIGNEE, JIRA_FIELD_UPDATED, JIRA_FIELD_STATUS, JIRA_FIELD_PROJECT, JIRA_FIELD_RELEASE_DATE));

		if(isset($this->data->issues))
		{
			foreach ($this->data->issues as $key => $value)
			{
				$dates[$value->fields->project->key][$value->fields->status->name][] = $value->fields->{JIRA_FIELD_RELEASE_DATE};
				$this->history[$value->fields->project->key][JIRA_STATUS_RELEASED][TOTAL]++;
				$this->history[$value->fields->project->key][JIRA_STATUS_RELEASED][TICKETS][$value->key] = array($value->fields->assignee->name => $value->fields->summary);
			}
			
			foreach($dates as $eachdatekey => $eachdatevalue)
				foreach($eachdatevalue as $key => $value)	
				{
					$this->history[$eachdatekey][$key][NEXT_RELEASE] =  min($eachdatevalue[$key]);		
					$this->history[$eachdatekey][$key][LAST_RELEASE] =  max($eachdatevalue[$key]);
				}
		}		
				
	}

	function get_released()
	{
		$this->_load(JIRA_QUERY_GET_RELEASED, array(JIRA_FIELD_ASSIGNEE, JIRA_FIELD_UPDATED, JIRA_FIELD_STATUS, JIRA_FIELD_PROJECT, JIRA_FIELD_RELEASE_DATE));
		//p(count($this->data->issues),JIRA_QUERY_GET_RELEASED);
		if(isset($this->data->issues))
		{
			foreach ($this->data->issues as $key => $value)
			{
			//	$dump[$value->fields->project->key] = $value->fields->{JIRA_FIELD_RELEASE_DATE};
				// Get Release Data (field "customfield_10038			
				$dates[$value->fields->project->key][$value->fields->status->name][] = $value->fields->{JIRA_FIELD_RELEASE_DATE};

				foreach($dates as $eachdatekey => $eachdatevalue) 
					foreach($eachdatevalue as $key => $value) 
						$this->history[$eachdatekey][$key][RELEASED_LAST] =  max($eachdatevalue[$key]);
			}
				
		}
		//p($dump);
	}

	public static function get_last_release($channel)
	{
		$fields = array(JIRA_FIELD_ASSIGNEE, JIRA_FIELD_UPDATED, JIRA_FIELD_STATUS, JIRA_FIELD_PROJECT, JIRA_FIELD_RELEASE_DATE);
		$jql = 'project = "'.$channel.'" and status = Released and "Release Date" is not EMPTY AND type = video ORDER BY "Release Date" desc';
		$data = array('jql' => $jql,
					  'fields' => $fields,
					  'maxResults' => 1
					  );

		$result = get_from('search', $data);

		return($result);
	}

	function _load($jql, $fields = false, $resource = 'search')
	{
		$data = array('jql' => $jql,
					  'fields' => $fields,
					  'maxResults' => 1000
					  );

		$this->data = get_from($resource, $data);

	}

	function get_recent_release()
	{
		;
	}

	function _get($jql)
	{
		// GET JQL and store in $this->data
		$this->_load($jql, array(JIRA_FIELD_ASSIGNEE, JIRA_FIELD_UPDATED, JIRA_FIELD_STATUS, JIRA_FIELD_PROJECT, JIRA_FIELD_RELEASE_DATE));

		// Go on each issue, count it and divide into channel, status, totals and tickets
		$this->history[$value->fields->project->key][$value->fields->status->name][TOTAL] = 0;
		if(isset($this->data))
			foreach ($this->data->issues as $key => $value) 
			{
				$this->history[$value->fields->project->key][$value->fields->status->name][TOTAL]++;
				$this->history[$value->fields->project->key][$value->fields->status->name][TICKETS][$value->key] = $value->fields->assignee->name;
			}
		
	}

}

Class Reviews extends Dashboard
{
	public $videos;
	public $channels;

	function __construct()
	{	
		$this->get_videos();
		$this->get_channels();
		$this->get_comments();
	}

	function get_channels()
	{
		if(isset($this->history))
			foreach ($this->history as $key => $value) {
				$this->channels[$value[JIRA_FIELD_PROJECT]->{JIRA_FIELD_KEY}] = $value[JIRA_FIELD_PROJECT]->{JIRA_FIELD_PROJECT_NAME};
		}
	}

	function get_video($video_key)
	{
		if(isset($this->history))
			foreach ($this->history as $key => $value) {
				if($video_key == $key)
					return $value;
		}
	}

	function get_videos()
	{
		//p(JIRA_QUERY_GET_READY_FOR_REVIEW);
		$this->_load(JIRA_QUERY_GET_READY_FOR_REVIEW, 
			array(JIRA_FIELD_COMMENT,
				JIRA_FIELD_SUMMARY,
				JIRA_FIELD_DESCRIPTION,
				JIRA_FIELD_ASSIGNEE, 
				JIRA_FIELD_UPDATED, 
				JIRA_FIELD_STATUS, 
				JIRA_FIELD_PROJECT, 
				JIRA_FIELD_RELEASE_DATE,
				JIRA_FIELD_YOUTUBE_URL,
				JIRA_FIELD_VIDEO_SOURCE,
				JIRA_FIELD_AUDIO_SOURCE,
				JIRA_FIELD_DESCRIPTION_LIST,
				JIRA_FIELD_YOUTUBE_DESCRIPTION,
				JIRA_FIELD_TAGS,
				JIRA_FIELD_YOUTUBE_TAGS,
				JIRA_FIELD_PRIMARY_KEYWORD,
				JIRA_FIELD_RELEASE_DATE,
				JIRA_FIELD_SECONDARY_KEYWORD,
				JIRA_FIELD_KEYWORDS_TITLE,
				JIRA_FIELD_CALL_TO_ACTION,
				JIRA_FIELD_YOUTUBE_METATAG,
				JIRA_FIELD_FINAL_VIDEO_LINK,
				JIRA_FIELD_GOOGLE_URL
				));
//p($this->data);
		if(isset($this->data->issues))
		foreach ($this->data->issues as $key => $value) 
		{

			$avatar = (array)$value->fields->assignee->{JIRA_FIELD_ASSIGNEE_AVATAR};
			$this->history[$value->{JIRA_FIELD_KEY}] = array(JIRA_FIELD_SUMMARY => $value->fields->{JIRA_FIELD_SUMMARY}, 
															JIRA_FIELD_KEY => $value->{JIRA_FIELD_KEY},
															JIRA_FIELD_DESCRIPTION => $value->fields->{JIRA_FIELD_DESCRIPTION},
															JIRA_FIELD_ASSIGNEE => $value->fields->assignee,
															JIRA_FIELD_PROJECT => $value->fields->{JIRA_FIELD_PROJECT},
															JIRA_FIELD_YOUTUBE_URL => $value->fields->{JIRA_FIELD_YOUTUBE_URL},
															JIRA_FIELD_VIDEO_SOURCE => $value->fields->{JIRA_FIELD_VIDEO_SOURCE},
															JIRA_FIELD_AUDIO_SOURCE => $value->fields->{JIRA_FIELD_AUDIO_SOURCE},
															JIRA_FIELD_DESCRIPTION_LIST => $value->fields->{JIRA_FIELD_DESCRIPTION_LIST},
															JIRA_FIELD_RELEASE_DATE => $value->fields->{JIRA_FIELD_RELEASE_DATE},
															JIRA_FIELD_YOUTUBE_DESCRIPTION => $value->fields->{JIRA_FIELD_YOUTUBE_DESCRIPTION},
															JIRA_FIELD_TAGS => $value->fields->{JIRA_FIELD_TAGS},
															JIRA_FIELD_YOUTUBE_TAGS => $value->fields->{JIRA_FIELD_YOUTUBE_TAGS},
															JIRA_FIELD_PRIMARY_KEYWORD => $value->fields->{JIRA_FIELD_PRIMARY_KEYWORD},
															JIRA_FIELD_KEYWORDS_TITLE => $value->fields->{JIRA_FIELD_KEYWORDS_TITLE},
															JIRA_FIELD_SECONDARY_KEYWORD => $value->fields->{JIRA_FIELD_SECONDARY_KEYWORD},
															JIRA_FIELD_CALL_TO_ACTION => $value->fields->{JIRA_FIELD_CALL_TO_ACTION},
															JIRA_FIELD_GOOGLE_URL => $value->fields->{JIRA_FIELD_GOOGLE_URL},
															JIRA_FIELD_YOUTUBE_METATAG => $value->fields->{JIRA_FIELD_YOUTUBE_METATAG},
															JIRA_FIELD_FINAL_VIDEO_LINK => $value->fields->{JIRA_FIELD_FINAL_VIDEO_LINK},
															JIRA_FIELD_STATUS => $value->fields->{JIRA_FIELD_STATUS},
															JIRA_FIELD_UPDATED => $value->fields->{JIRA_FIELD_UPDATED},
															JIRA_FIELD_COMMENT => $value->fields->{JIRA_FIELD_COMMENT});
		
		}
		
		$this->videos = $this->history;
	}
	
	function get_comments()
	{
		if(isset($this->history))
			foreach ($this->history as $key => $value)
				foreach ($value[JIRA_FIELD_COMMENT]->{JIRA_FIELD_COMMENTS} as $commentkey => $commentvalue)
				{

					$this->comments[] = $commentvalue;
					//$commentvalue;
				}
	}
}
Class Releases extends Dashboard
{
	public $videos;
	public $channels;
	
	function __construct()
	{	
		$this->get_videos();
		$this->get_channels();
	}
	
	function get_video($video_key)
	{
		if(isset($this->history))
			foreach ($this->history as $key => $value) {
				if($video_key == $key)
					return $value;
			}
	}

	function get_attachments($video_key)
	{
		return _get_attachments($video_key);
	}

	function get_channels()
	{
		if(isset($this->history))
			foreach ($this->history as $key => $value)
				$this->channels[$value[JIRA_FIELD_PROJECT]->{JIRA_FIELD_KEY}] = $value[JIRA_FIELD_PROJECT]->{JIRA_FIELD_PROJECT_NAME};
	}

	function add_tags($tags = array())
	{
		
		foreach ($tags as $key => $value) {			
			if($key !=0)
				$output .= ",";

			$output .= $value;
		}

		return $output;
	}



	function get_videos()
	{
		$this->_load(JIRA_QUERY_GET_READY_FOR_RELEASE_AND_PREPARE_FOR_RELEASE, 
			array(JIRA_FIELD_COMMENTS,
				JIRA_FIELD_SUMMARY,
				JIRA_FIELD_DESCRIPTION,
				JIRA_FIELD_ASSIGNEE, 
				JIRA_FIELD_UPDATED, 
				JIRA_FIELD_STATUS, 
				JIRA_FIELD_PROJECT, 
				JIRA_FIELD_RELEASE_DATE,
				JIRA_FIELD_YOUTUBE_URL,
				JIRA_FIELD_VIDEO_SOURCE,
				JIRA_FIELD_AUDIO_SOURCE,
				JIRA_FIELD_DESCRIPTION_LIST,
				JIRA_FIELD_YOUTUBE_DESCRIPTION,
				JIRA_FIELD_TAGS,
				JIRA_FIELD_YOUTUBE_TAGS,
				JIRA_FIELD_PRIMARY_KEYWORD,
				JIRA_FIELD_RELEASE_DATE,
				JIRA_FIELD_SECONDARY_KEYWORD,
				JIRA_FIELD_KEYWORDS_TITLE,
				JIRA_FIELD_CALL_TO_ACTION,
				JIRA_FIELD_YOUTUBE_METATAG,
				JIRA_FIELD_FINAL_VIDEO_LINK,
				JIRA_FIELD_GOOGLE_URL
				));

		if(isset($this->data->issues))
			foreach ($this->data->issues as $key => $value) 
			{

				$avatar = (array)$value->fields->assignee->{JIRA_FIELD_ASSIGNEE_AVATAR};
				$this->history[$value->{JIRA_FIELD_KEY}] = array(JIRA_FIELD_SUMMARY => $value->fields->{JIRA_FIELD_SUMMARY}, 
																JIRA_FIELD_KEY => $value->{JIRA_FIELD_KEY},
																JIRA_FIELD_DESCRIPTION => $value->fields->{JIRA_FIELD_DESCRIPTION},
																JIRA_FIELD_ASSIGNEE => $value->fields->assignee,
																JIRA_FIELD_PROJECT => $value->fields->{JIRA_FIELD_PROJECT},
																JIRA_FIELD_YOUTUBE_URL => $value->fields->{JIRA_FIELD_YOUTUBE_URL},
																JIRA_FIELD_VIDEO_SOURCE => $value->fields->{JIRA_FIELD_VIDEO_SOURCE},
																JIRA_FIELD_AUDIO_SOURCE => $value->fields->{JIRA_FIELD_AUDIO_SOURCE},
																JIRA_FIELD_DESCRIPTION_LIST => $value->fields->{JIRA_FIELD_DESCRIPTION_LIST},
																JIRA_FIELD_RELEASE_DATE => $value->fields->{JIRA_FIELD_RELEASE_DATE},
																JIRA_FIELD_YOUTUBE_DESCRIPTION => $value->fields->{JIRA_FIELD_YOUTUBE_DESCRIPTION},
																JIRA_FIELD_TAGS => $value->fields->{JIRA_FIELD_TAGS},
																JIRA_FIELD_YOUTUBE_TAGS => $value->fields->{JIRA_FIELD_YOUTUBE_TAGS},
																JIRA_FIELD_PRIMARY_KEYWORD => $value->fields->{JIRA_FIELD_PRIMARY_KEYWORD},
																JIRA_FIELD_KEYWORDS_TITLE => $value->fields->{JIRA_FIELD_KEYWORDS_TITLE},
																JIRA_FIELD_SECONDARY_KEYWORD => $value->fields->{JIRA_FIELD_SECONDARY_KEYWORD},
																JIRA_FIELD_CALL_TO_ACTION => $value->fields->{JIRA_FIELD_CALL_TO_ACTION},
																JIRA_FIELD_GOOGLE_URL => $value->fields->{JIRA_FIELD_GOOGLE_URL},
																JIRA_FIELD_YOUTUBE_METATAG => $value->fields->{JIRA_FIELD_YOUTUBE_METATAG},
																JIRA_FIELD_FINAL_VIDEO_LINK => $value->fields->{JIRA_FIELD_FINAL_VIDEO_LINK},
																JIRA_FIELD_STATUS => $value->fields->{JIRA_FIELD_STATUS},
																JIRA_FIELD_UPDATED => $value->fields->{JIRA_FIELD_UPDATED},
																JIRA_FIELD_COMMENTS => $value->fields->{JIRA_FIELD_COMMENTS});
			
			}
		
	}
}

class Database 
{
	private static $dbName = DB_BETUBE_NAME;
	private static $dbHost = DB_HOST;
	private static $dbUsername = DB_USER;
	private static $dbUserPassword = DB_PASS;
	
	private static $cont  = null;
	
	public function __construct() {
		exit('Init function is not allowed');
	}
	
	public static function connect()
	{
	   // One connection through whole application
       if ( null == self::$cont )
       {      
        try 
        {
          self::$cont =  new PDO( "mysql:host=".self::$dbHost.";"."dbname=".self::$dbName, self::$dbUsername, self::$dbUserPassword);  
        }
        catch(PDOException $e) 
        {
          die($e->getMessage());  
        }
       } 
       return self::$cont;
	}
	
	public static function disconnect()
	{
		self::$cont = null;
	}

	public static function get($table, $fields = "*", $where = "1"){
		$pdo = Database::connect();
	    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
	    
	    $sql = "SELECT ".$fields.
	    		" FROM ".$table.
	    		" Where ".$where;
	  //  p($sql);
	    $q = $pdo->prepare($sql);
	   	
	    $q->execute();
	    
	    $data = $q->fetchAll();
	    
	    Database::disconnect();

	    return $data;
	}

	public static function save($table, $values){
		$pdo = Database::connect();
	    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
	    
	    $sql = "REPLACE INTO ".$table.
	    		" SET ".
	    		$values;
	     p($sql);
	    $q = $pdo->prepare($sql);
	   
	    $q->execute();
	    
	    Database::disconnect();
	    return $q->rowCount();
	}

	public static function get_branding_tags_by_jira_key($jira_key)
	{
		$data = self::get("Channels", "*", "jira_key = '".$jira_key."'");
	//	p($data);
		return $data[0];
	}

	public static function get_channel_data($jira_key)
	{
		$data = self::get("Channels", "*", "jira_key = '".$jira_key."'");
	//	p($data);
		return $data[0];
	}
}


Class Slack
{
	public $users = array();

	function __construct()
	{
		$this->get_users();
		$this->get_active_users();
	}

	function get_users()
	{
		$this->users["list"] = get_from_slack("users.list");
	}

	function get_active_users()
	{
		if(isset($this->users["list"]->members))
			foreach ($this->users["list"]->members as $key => $value)
			{
				if(empty($value->deleted))
			 		$this->users["active"][] = $value;
			 	

			 }
	}
}

?>