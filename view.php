<?php
class View
{
    private $model;
    private $controller;
    private $output;

    public function __construct($controller,$model) {
        $this->controller = $controller;
        $this->model = $model;
        
        $panel1 = _html_dropdown("Channels","Adrenaline");
        $panel1 .= "</br> More content";
        $this->output = _html_header("Testando");
        $this->output .= _html_panel("Panel 1", $panel1);
        $this->output .= _html_panel("Panel 2", "Isso é dentro do panel 2");
        $this->output = _html_container($this->output);

    }
	
    function _html_header($header)
    {
        return '<div class="page-header">
            <h1>'.$header.'</h1>
        </div>';
    }
    function _html_dropdown($title, $items){
        return '<div class="dropdown">
                  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    $title
                    <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li><a href="#">$items</a></li>
                  </ul>
                </div>';
    }
    function _html_panel($heading,$content){
        return '<div id="$id" class="panel panel-default">
                    <div class="panel-heading">$heading</div>
                    <div class="panel-body">
                    $content
            </div>
        </div>';
    }

    function _html_container($content){
        return '<div class="container">$content</div>';
    }

    public function output(){
        return $this->output;
    }
}
?>