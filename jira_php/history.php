<?php
//pull in login credentials and CURL access function
 require_once("jira_php/utils.php");

function p($string) {
	print("<pre>");
	print_r($string);
	print("</pre>");
}

function search_issue($issue) {
	return get_from('search', $issue);
}

$inProgress = get_changelogs('EM-84');

//p($inProgress->changelog);

//p($inProgress->issues->fields->updated);

//check for errors
if (property_exists($inProgress, 'errors')) {
	echo "Error(s) searching for issues:\n";
	var_dump($inProgress);
} else {
	//print out the issue keys and summaries
	echo "Issues in Progress:<br/>";
	foreach ($inProgress->changelog->histories as $key => $value) {	
    	echo $value->created.": <br/>";
    	//.
    	foreach($value->items as $item){
    		echo $item->fromString." to ".$item->toString."</br>";
    	}
    	echo "</br>";
   	}
}

?>