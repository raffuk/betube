<?php 
error_reporting( error_reporting() & ~E_NOTICE );
require_once("credentials.php");

function put_to($resource, $data) {
	$jdata = json_encode($data);
	//p($jdata);
	$ch = curl_init();
	$url = JIRA_URL . '/rest/api/latest/' . $resource;
	// p($url,url);
	// p(json_encode($data),data);
	curl_setopt_array($ch, array(
		CURLOPT_CUSTOMREQUEST=>"PUT",
		CURLOPT_URL => $url,
		CURLOPT_USERPWD => USERNAME . ':' . PASSWORD,
		CURLOPT_POSTFIELDS => $jdata,
		CURLOPT_HTTPHEADER => array(
			'Accept: application/json',
			'Content-Type: application/json'
			),
		CURLOPT_RETURNTRANSFER => true
	));
	//print_r($ch);
	$result = curl_exec($ch);
	curl_close($ch);
	return json_decode($result);
}


function post_to($resource, $data) {
	$jdata = json_encode($data);
	$ch = curl_init();
	$url = JIRA_URL . '/rest/api/latest/' . $resource;
	// p($url, url);
	// p($jdata,jdata);
	//exit;
	curl_setopt_array($ch, array(
		CURLOPT_POST => 1,
		CURLOPT_URL => $url,
		CURLOPT_USERPWD => USERNAME . ':' . PASSWORD,
		CURLOPT_POSTFIELDS => $jdata,
		CURLOPT_HTTPHEADER => array('Content-type: application/json'),
		CURLOPT_RETURNTRANSFER => true
	));
	$result = curl_exec($ch);
	curl_close($ch);
	return json_decode($result);
}

function get_from_slack($resource){
	//$resource can be "users.list"
	//convert array to JSON string
	$ch = curl_init();
	$url = SLACK_URL . '/api/' . $resource."?token=".SLACK_TOKEN."&pretty=1";
//	p($url,$jdata);
	//configure CURL
	curl_setopt_array($ch, array(
		CURLOPT_URL => $url,
		CURLOPT_HTTPHEADER => array('Content-type: application/json'),
		CURLOPT_RETURNTRANSFER => true
	));
	$result = curl_exec($ch);
	curl_close($ch);
	//convert JSON data back to PHP array
	//p($result,$url);
	return json_decode($result);
}



function get_from($resource, $data) {
	//convert array to JSON string
	$jdata = json_encode($data);
	$ch = curl_init();
	$url = JIRA_URL . '/rest/api/latest/' . $resource;
	//p($url,$jdata);
	//configure CURL
	curl_setopt_array($ch, array(
		CURLOPT_URL => $url,
		CURLOPT_USERPWD => USERNAME . ':' . PASSWORD,
		CURLOPT_POSTFIELDS => $jdata,
		CURLOPT_HTTPHEADER => array('Content-type: application/json'),
		CURLOPT_RETURNTRANSFER => true
	));
	$result = curl_exec($ch);
	curl_close($ch);
	//convert JSON data back to PHP array
	//p(json_decode($result),"here");
	return json_decode($result);
}

function _get_attachments($issue) {
	
	//convert array to JSON string
	$url = JIRA_URL . '/rest/api/2/issue/' . $issue."?fields=attachment";
	$ch = curl_init();
	
	//configure CURL
	curl_setopt_array($ch, array(
		CURLOPT_URL => $url,
		CURLOPT_USERPWD => USERNAME . ':' . PASSWORD,
		CURLOPT_HTTPHEADER => array('Content-type: application/json'),
		CURLOPT_RETURNTRANSFER => true
	));

	$result = curl_exec($ch);
	curl_close($ch);
	//convert JSON data back to PHP array
	return json_decode($result);
}

function get_changelogs($issue) {
	//convert array to JSON string
	$url = JIRA_URL . '/rest/api/2/issue/' . $issue."?expand=changelog";
	
	$ch = curl_init();
	//configure CURL
	curl_setopt_array($ch, array(
		CURLOPT_URL => $url,
		CURLOPT_USERPWD => USERNAME . ':' . PASSWORD,
		CURLOPT_HTTPHEADER => array('Content-type: application/json'),
		CURLOPT_RETURNTRANSFER => true
	));
	$result = curl_exec($ch);
	curl_close($ch);
	//convert JSON data back to PHP array
	return json_decode($result);
}
?>