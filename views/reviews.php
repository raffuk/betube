<!DOCTYPE html>
<html lang="en">
<head>
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BeTube 1.0</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="gridstack.js/dist/gridstack.css"/>
     <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.0/jquery-ui.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.5.0/lodash.min.js"></script>
    <script src="gridstack.js/dist/gridstack.js"></script>
    <script src="gridstack.js/dist/gridstack.jQueryUI.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.min.js"></script>
    
    <style type="text/css">

    /*
		WHITE: #FEFDFF
		SILVER: #C1BFB5
		ORANGE RED: #EB4511
		RED RUST: #B02E0C
		PEWTER BLUE: #8EB1C7


    */
        .grid-stack {
            background: #FEFDFF;
        }

        .grid-stack-item-content {
            color: #2c3e50;
            text-align: center;
            background-color: #C1BFB5;
        }

        .grid-stack .grid-stack {
            /*margin: 0 -10px;*/
            background: #E8E7E4;
        }

        .grid-stack .grid-stack .grid-stack-item-content {
            background: #F7BBA8;
        }

    	#in_progress_assignees {
		    display: none;
		}
		 	.alert-warning{
				font-size:11px;
				text-align:left;
				padding: 5px;
			}
			body{
		 		font-size:12px;
			}
		  #panel{
		    display:block;
		  }
		  #videopreparation{
		    float:left;
		    width: 50%;
		  }

		  #videopreview{
		    float:right;
		    width: 50%;
		  }

		  .container{
		    width: 100%;
		    padding: 5px;

		  }

		  .videoprep{
		    float:left;
		    width: 50%;
		  }
		  .videopreview{
		    float:right;
		    /*width: 50%;*/
		  }
		  
		  #youtube_preview {
		    min-height: 20%;
		  }
		.keywords {
		    width: 50%;
		    float: left;
		}
		#save_jira {
		  width: 100%;
		  height: 60px;
		}
		.updatejira {
		    float: right;
		    display: block;
		    max-width: 50%;
		    height: 60px;
		}
		.panel-title .prep{
		  float:left;
		}

		.sources{
		  float:right;
		  width: 50%;
		  display: block;
		}
		.sources button{
		  width: 100%;
		}

		.list select{
		  width: 100%;
		}

		.list{
		  float:left;
		  width: 50%;
		  display: block;
		}
		
		textarea, input, select{
		  font-size:11px !important;
		}
		
		button {
		  font-size:12pt !important;
		}
		.glyphicon {
		  
		  cursor: pointer;
		}
		
		select .selectpicker{
		  width: 100%;
		  display: block;
		}
		.visuals{
		  display: block;
		  width: 100%;
		  padding:3;
		}

		.form-group .youtube{
		  display: block;
		  float:left;
		  width: 70%;

		}
		.form-group .images{
		  display: block;
		  float:right;
		  width: 30%;
		  padding: 3px;
		}
		.form-group .images img{  
		  width: 100%;
		  height: 100%;
		}
		  .carousel-inner>.item>a>img, .carousel-inner>.item>img {
        display: block;
        width:290px;
        height:150px;
        max-width: auto;
        max-height: auto;
      }
		
		.grid-stack-item-content{
			padding:10px;
		}
		textarea.form-control.reviews{
			height: 100px;
		}
		p.comments{
			text-align: left;
			display: block;
			background-color: white;
		}
		img.avatar{
			max-width: 48px;
			max-height: 48px;
		}
    </style>
</head>

<?php
require_once("constants.php");
//pull in login credentials and CURL access function
require_once("jira_php/utils.php");
//call all the functions 
require_once("functions.php");
require_once("classes.php");

//p($_POST);
$reviews = new Reviews();
if(empty($reviews->videos))
	exit("<h3>No Videos to Review</h3>");
if(!empty($_POST))
{
	if($_POST["Reject"])
	{
		Dashboard::change_status_in_jira($_POST[JIRA_FIELD_KEY],JIRA_TRANSITION_READY_FOR_REVIEW_REJECTED,$_POST["comment"]);
	}
	elseif($_POST["Approve"])
	{
		//$reviews->save_jira_comment($video[JIRA_FIELD_KEY],$_POST["comment"]);
		Dashboard::change_status_in_jira($_POST[JIRA_FIELD_KEY],JIRA_TRANSITION_PREPARE_FOR_RELEASE,$_POST["comment"]);
	}
	elseif ($_POST["Needs_Executive_Approval"]) {
		Dashboard::change_status_in_jira($_POST[JIRA_FIELD_KEY],JIRA_TRANSITION_NEEDS_EXECUTIVE_APPROVAL,$_POST["comment"]);
	}

	if($_POST[JIRA_FIELD_VIDEO_OF_THE_MONTH])
		Dashboard::save_in_jira($_POST[JIRA_FIELD_KEY],array(JIRA_FIELD_VIDEO_OF_THE_MONTH => array(array("value" => JIRA_FIELD_VIDEO_OF_THE_MONTH_ON))));	
	
	$reviews = new Reviews();
	$default_video = reset($reviews->videos);
	$video = $reviews->get_video($default_video[JIRA_FIELD_KEY]);

}
if($_GET["video"])
	$video = $reviews->get_video($_GET["video"]);
		
if((empty($_GET["video"])) || (empty($video))){
		$default_video = reset($reviews->videos);
		$video = $reviews->get_video($default_video[JIRA_FIELD_KEY]);
		//p($video,$video[JIRA_FIELD_YOUTUBE_URL]);
}

	$avatar = (array) $video[JIRA_FIELD_ASSIGNEE]->{JIRA_FIELD_ASSIGNEE_AVATARURLS};
	//p($avatar);
	//p($ticket[JIRA_FIELD_ASSIGNEE]->{JIRA_FIELD_ASSIGNEE_AVATARURLS});
	//p($ticket[JIRA_FIELD_ASSIGNEE]->{JIRA_FIELD_ASSIGNEE_AVATARURLS}->{'48X48'});
	$avatar_size = "48";
	$JIRA_FIELD_ASSIGNEE_AVATAR = '<img class="avatar" title="'.$video[JIRA_FIELD_ASSIGNEE]->{JIRA_FIELD_ASSIGNEE_DISPLAYNAME}.'"src="'.$avatar[$avatar_size."x".$avatar_size].'" class="img-circle" alt="Cinque Terre" width="$avatar_size" height="$avatar_size">';

	if(isset($video[JIRA_FIELD_KEY]))
		$subtitle =  $JIRA_FIELD_ASSIGNEE_AVATAR." ".$video[JIRA_FIELD_KEY].": ".$video[JIRA_FIELD_SUMMARY]." - ".$video[JIRA_FIELD_STATUS]->{JIRA_STATUS_NAME}." (".count($video[JIRA_FIELD_COMMENT]->{JIRA_FIELD_COMMENTS})." comment(s))";

?>
<body>

  <div class="container-fluid">
        <h1>BeTube</h1>
		<form id="video" name="video" method="GET" action="">
	        <?php echo _inject_dropdown_with_group($reviews, "video", $_GET["video"]); ?>
	    </form>

	    <h3><?php
	 //   p($video);
echo $subtitle
			
	    ?></h3>
	    <form id="review" name="review" method="POST" action="">
	        <div class="grid-stack">
	            <div class="grid-stack-item" data-gs-x="20" data-gs-y="0" data-gs-width="7" data-gs-height="8">
	                <div class="grid-stack-item-content">
	        		 <?php echo embedYoutube($video[JIRA_FIELD_YOUTUBE_URL]); ?>
	                </div>
	            </div>
	            <div class="grid-stack-item" data-gs-x="0" data-gs-y="0" data-gs-width="5" data-gs-height="8">
	                <div class="grid-stack-item-content">
	                	<div class="grid-stack"> 
	                        <div class="grid-stack-item" data-gs-x="0" data-gs-y="0" data-gs-width="90%" data-gs-height="5">
	                        	<div class="grid-stack-item-content">
	                        		<?php

	                        		if($video[JIRA_FIELD_COMMENT]->{JIRA_FIELD_COMMENTS})
	                        		foreach ($video[JIRA_FIELD_COMMENT]->{JIRA_FIELD_COMMENTS} as $key => $value) {
	                        			echo "<p class='comments' title='".$value->updateAuthor->name.": ".$value->updated."'>".$value->updateAuthor->name.": ".$value->body."</p>";
	                        		}
	                        		?>
	                        	</div>
	                        </div>
	                        <div class="grid-stack-item" data-gs-x="0" data-gs-y="10" data-gs-width="90%" data-gs-height="3">
	                        	<div class="grid-stack-item-content">
	                        		<?php echo _formgroupTextarea("comment",$_GET[JIRA_FIELD_REVIEW],10,5,"reviews");?>
	                        		<?php echo _injectHiddenField(JIRA_FIELD_KEY,$video[JIRA_FIELD_KEY])?>
	                        		<?php echo _formgroupCheckbox(JIRA_FIELD_VIDEO_OF_THE_MONTH, "Candidate to video of the month?");?>
	                        		<?php echo _formgroupSubmitbutton("Reject");?>
	                        		<?php echo _formgroupSubmitbutton("Approve");?>
	                        		<?php echo _formgroupSubmitbutton("Needs Executive Approval");?>
	                        		

	                        	</div>
	                	    </div>                            
	                    </div>
	                </div>
	            </div>
	            <!-- <div class="grid-stack-item" data-gs-x="4" data-gs-y="0" data-gs-width="4" data-gs-height="4">
	                <div class="grid-stack-item-content">
	                            2nd
	                        <div class="grid-stack">
	                            <div class="grid-stack-item" data-gs-x="0" data-gs-y="0" data-gs-width="3" data-gs-height="1"><div class="grid-stack-item-content">1</div></div>
	                            <div class="grid-stack-item" data-gs-x="3" data-gs-y="0" data-gs-width="3" data-gs-height="1"><div class="grid-stack-item-content">2</div></div>
	                            <div class="grid-stack-item" data-gs-x="6" data-gs-y="0" data-gs-width="3" data-gs-height="1"><div class="grid-stack-item-content">3</div></div>
	                            <div class="grid-stack-item" data-gs-x="9" data-gs-y="0" data-gs-width="3" data-gs-height="1"><div class="grid-stack-item-content">4</div></div>
	                            <div class="grid-stack-item" data-gs-x="0" data-gs-y="1" data-gs-width="3" data-gs-height="1"><div class="grid-stack-item-content">5</div></div>
	                            <div class="grid-stack-item" data-gs-x="3" data-gs-y="1" data-gs-width="3" data-gs-height="1"><div class="grid-stack-item-content">6</div></div>
	                        </div>

	                </div> -->
	            </div>
	        </div>
        </form>
    </div>


    <script type="text/javascript">
        $(function () {
            var options = {
            };
            $('.grid-stack').gridstack(options);
        });
    </script>
</body>
</html>
