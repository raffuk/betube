var channelSettings = {	"Adrenaline Channel" : {
							"tags" : "EMC,Adrenaline,Adrenaline Bike,Adrenaline Climbing,Adrenaline Fails,Adrenaline Fight,Adrenaline Flying,Adrenaline Fun,Adrenaline Girls,Adrenaline Kite,Adrenaline Highs,Adrenaline Longboard,Adrenaline Legends,Adrenaline Motorcycle,Adrenaline Parkour,Adrenaline Racing,Adrenaline Skate,Adrenaline Street,Adrenaline Snow,Adrenaline Water Sports,Adrenaline Wingsuit,Adrenaline Channel",
							"brand": "[Adrenaline Channel]",
							"releaseDays": "Tuesday, Thursday, Saturday & Sunday 7:00AM",
							"channelLink" : "http://bit.ly/AdrenalineChannel",
							"facebookLink" : "http://bit.ly/AdrenalineChannelFB",
							"bestIn" : "extreme sports and adrenaline videos",
							"headLight" : "Check this video out!"
						},
						"Epic Fails" : {
							"tags" : "EMC, Funny Fails, Sport Fails,CCTV Fails,Water Fails,Funny Fails,Dash Cam Fails, Epic Dash Cam, Epic Laughs, Construction Fails, Epic Fails",
							"brand": "[Epic Fails]",
							"releaseDays": "Saturday 9:00AM",
							"channelLink" : "http://bit.ly/EpicFailsChannel",
							"facebookLink" : "http://bit.ly/EpicFailsFB",
							"bestIn" : "Epic Fails videos",
							"headLight" : "Check this video out!"
						},"Epic Fitness Motivation" : {
							"tags" : "EMC,Epic Fitness Motivation",
							"brand": "[Epic Fitness Motivation]",
							"releaseDays": "?",
							"channelLink" : "?",
							"facebookLink" : "?",
							"bestIn" : "?",
							"headLight" : "?"
						},
						"Epic Food" : {
							"tags" : "EMC, Epic Food",
							"brand": "[Epic Food]",
							"releaseDays": "Sundays 8:00AM",
							"channelLink" : "http://bit.ly/EpicFood",
							"facebookLink" : "http://bit.ly/EpicFoodFB",
							"bestIn" : "",
							"headLight" : "."
						},
						"Epic Dash Cam" : {
							"tags" : "EMC,EDC Motorcycle,Adrenaline Motorcycle,EDC Trucks,EDC Boats,EDC Expensive,EDC Parking,EDC Lucky,EDC Police,EDC Airplanes,EDC Road Rage,EDC Idiots,EDC Trains,EDC SuperCars,EDC Animals,Epic Dash Cam",
							"brand": "[Epic Dash Cam]",
							"releaseDays": "Tuesday, Thursday & Saturday 9:00AM",
							"channelLink" : "http://bit.ly/EpicDashcams",
							"facebookLink" : "http://bit.ly/EpicDashcamsFB",
							"bestIn" : "shocking dash cam videos",
							"headLight" : "Watch at your own risk"
						},
						"Epic Girls" : {
							"tags" : "EMC, Epic Girls Europe, Epic Girls America,Epic Girls Asia,Epic Girls Beach,Epic Girls Models,Epic Girls Sports,Epic Girls Playboy,Epic Girls Places,Epic Girls Tattoo,Epic Girls Music,Epic Girls",
							"brand": "[Epic Girls]",
							"releaseDays": "Monday, Thursday & Saturday 12:00PM",
							"channelLink" : "http://bit.ly/EpicGirls",
							"facebookLink" : "http://bit.ly/EpicGirlsFacebook",
							"bestIn" : "beauty motivation",
							"headLight" : "If you like beautiful girl, you GOT TO WATCH THIS VIDEO"
						},
						"Epic Laughs" : {
							"tags" : "EMC,Epic Fail,Epic Dogs, Epic Cats, Epic Pets, Funny Pets, Epic Laughs",
							"brand": "[Epic Laughs]",
							"releaseDays": "Tuesday, Thursday, Friday, Saturday 9:00AM",
							"channelLink" : "http://bit.ly/EpicLaughsChannel",
							"facebookLink" : "http://bit.ly/EpicLaughsFB",
							"bestIn" : "funny videos",
							"headLight" : "Try NOT TO LAUGH at this video!"
						},
						"Epic Life" : {
							"tags" : "EMC,Epic Life Girls,Epic Life Money,Epic Life Cars,Epic Life Places,Epic Life",
							"brand": "[Epic Life]",
							"releaseDays": "Tuesday, Thursday, Saturday 9:00AM",
							"channelLink" : "http://bit.ly/EpicLifeChannel",
							"facebookLink" : "http://bit.ly/EpicLifeFB",
							"bestIn" : "billionaire lifestyle",
							"headLight" : "WATCH THIS if you want to be inspired!"
						},
						"Epic Lists" : {
							"tags" : "EMC,Adrenaline,Epic Cars,Epic Movies,Epic Music,Epic Games,Epic Celebrities,Epic Animals,Epic Tech,Epic Places,Epic Curiosity,Epic Lists",
							"brand": "[Epic Lists]",
							"releaseDays": "Wednesday, Friday & Saturday 12:00PM",
							"channelLink" : "http://bit.ly/EpicListsSubscribe",
							"facebookLink" : "https //bit.ly/EpicListsFB",
							"bestIn" : "top 10 lists",
							"headLight" : "Check this video out"
						},
						"Epic Method" : {
							"tags" : "EMC, Epic Method",
							"brand": "[Epic Method]",
							"releaseDays": "Thursday 9:00AM",
							"channelLink" : "http://bit.ly/EpicMethod",
							"facebookLink" : "http://bit.ly/EpicMethodFB",
							"bestIn" : "LIFE HACKS videos",
							"headLight" : "Amazing video."
						},
						"Epic Motivation" : {
							"tags" : "EMC, Epic Motivation",
							"brand": "[Epic Motivation]",
							"releaseDays": "Thursday 9:00AM",
							"channelLink" : "http://bit.ly/EpicMotivation",
							"facebookLink" : "http://bit.ly/EpicMotivationFB",
							"bestIn" : "motivational videos",
							"headLight" : "WATCH THIS if you want to be inspired"
						},
						"Epic Music" : {
							"tags" : "EMC, Epic Lists Music,Epic Music",
							"brand": "[Epic Music]",
							"releaseDays": "Thursday 9:00AM",
							"channelLink" : "http://bit.ly/EpicMusicChannel",
							"facebookLink" : "http://bit.ly/EpicMusicFB",
							"bestIn" : "music videos",
							"headLight" : "Relax and listen."
						},
						"Epic Play" : {
							"tags" : "EMC,Epic Play",
							"brand": "[Epic Play]",
							"releaseDays": "Any 9:00AM",
							"channelLink" : "http://bit.ly/EpicPlayChannel",
							"facebookLink" : "http://bit.ly/EpicPlayFB",
							"bestIn" : "Games",
							"headLight" : "Amazing Play!",
						},
						"Epic Surf" : {
							"tags" : "EMC,Epic Girls,Epic Music,Epic Surf Longboard,Epic Surf Girls,Epic Surf Spots,Epic Surf Legends,Epic Surf",
							"brand": "[Epic Surf]",
							"releaseDays": "Tuesday, Thursday, Saturday 9:00AM",
							"channelLink" : "http://bit.ly/EpicSurfChannel",
							"facebookLink" : "http://bit.ly/EpicSurfFB",
							"bestIn" : "Surf Videos",
							"headLight" : "Amazing Surfing Video!",
						},
						"Funny Pets" : {
							"tags" : "EMC, Trip Burger Pets, Epic Laughs, Trip Burger Laughs,Funny Monkeys,Funny Dogs,Funny Cats,Funny Horses,Funny Pets",
							"brand": "[Funny Pets]",
							"releaseDays": "Tuesday, Thursday, Friday, Saturday 9:00AM",
							"channelLink" : "http://bit.ly/FunnyPetMedia",
							"facebookLink" : "http://bit.ly/FunnyPetMediaFB",
							"bestIn" : "funny pets videos",
							"headLight" : "TRY NOT TO LAUGH AT THIS VIDEO!"
						},
						"TNT Channel" : {
							"tags" : "EMC,Epic Laughs, Funny Pets,TNT Motorcycle,Adrenaline Motorcycle,TNT Trucks,TNT Boats,TNT Expensive,TNT Parking,TNT Lucky,TNT Police,TNT Airplanes,TNT Road Rage,TNT Idiots,TNT Trains,TNT SuperCars,TNT Animals,TNT Channel",
							"brand": "[TNT Channel]",
							"releaseDays": "Tuesday, Thursday, Saturday & Sunday 9:00AM",
							"channelLink" : "http://bit.ly/TNTChannelTV",
							"facebookLink" : "http://bit.ly/TNTchannelFB",
							"bestIn" : "EPIC VIDEOS",
							"headLight" : "You GOT TO WATCH THIS.",
						},
						"Trip Burger Pets" : {
							"tags" : "EMC,TB Cats,TB Cute,TB Dogs,TB Pets,TB Fails,TB Wins,funny pets,Trip Burger Laughs,Trip Burger Pets",
							"brand": "[Trip Burger Pets]",
							"releaseDays": "Tuesday, Thursday & Saturday 9:00AM",
							"channelLink" : "http://bit.ly/TripBurgerPets",
							"facebookLink" : "http://bit.ly/TripBurgerPetsFB",
							"bestIn" : "funny videos",
							"headLight" : "TRY NOT TO LAUGH AT THIS VIDEO"
						},
						"Trip Burger Laughs" : {
							"tags" : "EMC,Epic Laughs,TB Fails,TB Wins,Trip Burger Pets,Trip Burger Laughs",
							"brand": "[Trip Burger Pets]",
							"releaseDays": "Tuesday, Thursday & Saturday 9:00AM",
							"channelLink" : "http://bit.ly/TripBurgerLaughs",
							"facebookLink" : "http://bit.ly/TripBurgerLaughsFB",
							"bestIn" : "funny videos",
							"headLight" : "TRY NOT TO LAUGH AT THIS VIDEO"
						}
					  };
